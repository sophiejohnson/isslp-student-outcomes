<!doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>Past ISSLP Participants</title>
        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <link href="https://fonts.googleapis.com/css?family=Nunito+Sans" rel="stylesheet">
        <link href="styles/styles.css?v=1.2" rel="stylesheet">
        <link href="styles/student_action.css?v=1.2" rel="stylesheet">

        <!-- Adds frosted glass effect -->
        <link href="styles/glass.css" rel="stylesheet">

        <!-- jQuery library -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <!-- Latest compiled JavaScript -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <!-- Load Navbar -->
        <script>
            $(function(){
                $("#includedContent").load("navbar.html");
            });
        </script>
    </head>
    <body>
        <!-- Navbar -->
        <div id="includedContent"></div>
        <?php
        $msg = "";
        // Add new student to database
        if (isset($_GET['action']) && $_GET["action"] == "insert") {
            // Connect to database
            $link = mysqli_connect('localhost', 'sjohns37', 'databases2018', 'sjohns37');
            if (mysqli_connect_errno()) {
                printf("Connect failed: %s\n", mysqli_connect_error());
                exit();
            }
            // Check if student already in database
            if ($stmt = mysqli_prepare($link, "SELECT is_deleted FROM students WHERE netid=?;")) {
                mysqli_stmt_bind_param($stmt, "s", $_GET["netid"]);
                mysqli_stmt_execute($stmt);
                mysqli_stmt_bind_result($stmt, $is_deleted);
                // If student exists, show error
                if (mysqli_stmt_fetch($stmt) && !$is_deleted) {
                    $msg = "<p>A student with the netID " . $_GET['netid'] . " is already in the database.</p>";
                    mysqli_stmt_close($stmt);
                }
                // If student does not exist or was previously deleted, continue with insertion
                else {
                    mysqli_stmt_close($stmt);
                    // Delete previously deleted entry if necessary
                    if ($is_deleted && ($stmt = mysqli_prepare($link, "DELETE FROM students WHERE netid=?;"))) {
                        mysqli_stmt_bind_param($stmt, "s", $_GET["netid"]);
                        mysqli_stmt_execute($stmt);
                    }
                    else {
                        $insert = false;
                    }
                    // Initialize variables
                    $query = "";
                    $query_vals = "";
                    $types = "";
                    $params = array();
                    $insert = false;

                    // Build query, datatypes, and parameters 
                    // Match to netid exactly
                    if (!empty($_GET['netid'])) {
                        $types .= "s";
                        $params[] = $_GET['netid']; 
                        $query .= "netid";
                        $query_vals .= "?";
                    }
                    // Match to first and last name as substrings
                    if (!empty($_GET['firstname'])) {
                        $types .= "s";
                        $params[] = $_GET['firstname']; 
                        if (!empty($query)) {
                            $query .= ", ";
                            $query_vals .= ", ";
                        }
                        $query .= "first_name";
                        $query_vals .= "?";
                    }
                    if (!empty($_GET['lastname'])) {
                        $types .= "s";
                        $params[] = $_GET['lastname']; 
                        if (!empty($query)) {
                            $query .= ", ";
                            $query_vals .= ", ";
                        }
                        $query .= "last_name";
                        $query_vals .= "?";
                    }
                    // ISSLP year
                    if (!empty($_GET['isslpyear'])) {
                        $types .= "i";
                        $params[] = $_GET['isslpyear']; 
                        if (!empty($query)) {
                            $query .= ", ";
                            $query_vals .= ", ";
                        }
                        $query .= "isslp_year";
                        $query_vals .= "?";
                    }
                    // ISSLP site
                    if (!empty($_GET['site'])) {
                        $types .= "i";
                        $params[] = $_GET['site']; 
                        if (!empty($query)) {
                            $query .= ", ";
                            $query_vals .= ", ";
                        }
                        $query .= "site_id";
                        $query_vals .= "?"; } 
                    // Grad year 
                    if (!empty($_GET['gradyear'])) { 
                        $types .= "i";
                        $params[] = $_GET['gradyear'];
                        if (!empty($query)) { 
                            $query .= ", ";
                            $query_vals .= ", ";
                        }
                        $query .= "grad_year";
                        $query_vals .= "?";
                    }
                    // Notes 
                    if (!empty($_GET['notes'])) { 
                        $types .= "s";
                        $params[] = $_GET['notes'];
                        if (!empty($query)) { 
                            $query .= ", ";
                            $query_vals .= ", ";
                        }
                        $query .= "notes";
                        $query_vals .= "?";
                    }
                    // Build query and store parameters in array (by reference)
                    $query = "INSERT INTO students (" . $query  . ") VALUES (" . $query_vals  . ");";
                    $bind_params = array();
                    $bind_params[] = & $types;
                    for ($i = 0; $i < count($params); $i++) {
                        $bind_params[] = & $params[$i];
                    }
                    // Prepare and execute query
                    if ($stmt = mysqli_prepare($link, $query)) {
                        call_user_func_array(array($stmt, 'bind_param'), $bind_params);
                        if (mysqli_stmt_execute($stmt)) {
                            $insert = true;
                        }
                        mysqli_stmt_close($stmt);
                    }
                    if ($insert) {
                        // Add isslp majors
                        if (!empty($_GET['isslpmajors'])) { 
                            $majors = $_GET['isslpmajors'];
                            foreach ($majors as $m) {
                                if ((!empty($m)) && ($stmt = mysqli_prepare($link, "INSERT INTO isslp_majors VALUES (?, ?)"))) {
                                    mysqli_stmt_bind_param($stmt, "si", $_GET['netid'], $m);
                                    mysqli_stmt_execute($stmt);
                                    mysqli_stmt_close($stmt);
                                }
                            } 
                        }
                        // Add isslp minors
                        if (!empty($_GET['isslpminors'])) { 
                            $minors = $_GET['isslpminors'];
                            foreach ($minors as $m) {
                                if ((!empty($m)) && ($stmt = mysqli_prepare($link, "INSERT INTO isslp_minors VALUES (?, ?)"))) {
                                    mysqli_stmt_bind_param($stmt, "si", $_GET['netid'], $m);
                                    mysqli_stmt_execute($stmt);
                                    mysqli_stmt_close($stmt);
                                }
                            } 
                        }
                        // Add grad majors
                        if (!empty($_GET['gradmajors'])) { 
                            $majors = $_GET['gradmajors'];
                            foreach ($majors as $m) {
                                if ((!empty($m)) && ($stmt = mysqli_prepare($link, "INSERT INTO grad_majors VALUES (?, ?)"))) {
                                    mysqli_stmt_bind_param($stmt, "si", $_GET['netid'], $m);
                                    mysqli_stmt_execute($stmt);
                                    mysqli_stmt_close($stmt);
                                }
                            } 
                        }
                    // Add grad minors
                        if (!empty($_GET['gradminors'])) { 
                            $minors = $_GET['gradminors'];
                            foreach ($minors as $m) {
                                if ((!empty($m)) && ($stmt = mysqli_prepare($link, "INSERT INTO grad_minors VALUES (?, ?)"))) {
                                    mysqli_stmt_bind_param($stmt, "si", $_GET['netid'], $m);
                                    mysqli_stmt_execute($stmt);
                                    mysqli_stmt_close($stmt);
                                }
                            } 
                        }
                        // Add phone numbers 
                        if (!empty($_GET['phones'])) { 
                            $phones = $_GET['phones'];
                            foreach ($phones as $p) {
                                if ((!empty($p)) && ($stmt = mysqli_prepare($link, "INSERT INTO student_phones VALUES (?, ?)"))) {
                                    mysqli_stmt_bind_param($stmt, "ss", $_GET['netid'], $p);
                                    mysqli_stmt_execute($stmt);
                                    mysqli_stmt_close($stmt);
                                }
                            } 
                        }
                        // Add email addresses 
                        if (!empty($_GET['emails'])) { 
                            $emails = $_GET['emails'];
                            foreach ($emails as $e) {
                                if ((!empty($e)) && ($stmt = mysqli_prepare($link, "INSERT INTO student_emails VALUES (?, ?)"))) {
                                    mysqli_stmt_bind_param($stmt, "ss", $_GET['netid'], $e);
                                    mysqli_stmt_execute($stmt);
                                    mysqli_stmt_close($stmt);
                                }
                            } 
                        }
                    }
                    // Display is insertion was successful
                    if ($insert) {
                        $msg = "<p>You have successfully inserted " . $_GET["firstname"] . " "  . $_GET["lastname"] . " ("  . $_GET["netid"] . ") into the database";
                    }
                    else {
                        $msg = "<p>Error. Unable to insert " . $_GET["firstname"] . " "  . $_GET["lastname"] . " ("  . $_GET["netid"] . ") into the database";
                    }
                }
            }
        }
        // Update student in the database
        if (isset($_GET['action']) && $_GET["action"] == "update") {
            // Connect to database
            $link = mysqli_connect('localhost', 'sjohns37', 'databases2018', 'sjohns37');
            if (mysqli_connect_errno()) {
                printf("Connect failed: %s\n", mysqli_connect_error());
                exit();
            }
            // Check if student already in database
            $query = "";
            $types = "";
            $params = array();
            $update = false;

            // Build query, datatypes, and parameters 
            // Match to first and last name as substrings
            if (!empty($_GET['firstname'])) {
                $types .= "s";
                $params[] = $_GET['firstname']; 
                if (!empty($query)) {
                    $query .= ", ";
                }
                $query .= "first_name=?";
            }
            if (!empty($_GET['lastname'])) {
                $types .= "s";
                $params[] = $_GET['lastname']; 
                if (!empty($query)) {
                    $query .= ", ";
                }
                $query .= "last_name=?";
            }
            // ISSLP year
            if (!empty($_GET['isslpyear'])) {
                $types .= "i";
                $params[] = $_GET['isslpyear']; 
                if (!empty($query)) {
                    $query .= ", ";
                }
                $query .= "isslp_year=?";
            }
            // ISSLP site
            if (!empty($_GET['site'])) {
                $types .= "i";
                $params[] = $_GET['site']; 
                if (!empty($query)) {
                    $query .= ", ";
                }
                $query .= "site_id=?";
            }
            // Grad year 
            if (!empty($_GET['gradyear'])) { 
                $types .= "i";
                $params[] = $_GET['gradyear'];
                if (!empty($query)) { 
                    $query .= ", ";
                }
                $query .= "grad_year=?";
            }
            // Notes 
            if (!empty($_GET['notes'])) { 
                $types .= "s";
                $params[] = $_GET['notes'];
                if (!empty($query)) { 
                    $query .= ", ";
                }
                $query .= "notes=?";
            }
            // Match to netid exactly
            if (!empty($_GET['netid'])) {
                $types .= "s";
                $params[] = $_GET['netid']; 
            }
            // Build query and store parameters in array (by reference)
            $update = true;
            $query = "UPDATE students SET " . $query . " WHERE netid=?;";
            $bind_params = array();
            $bind_params[] = & $types;
            for ($i = 0; $i < count($params); $i++) {
                $bind_params[] = & $params[$i];
            }
            // Prepare and execute query
            if ($stmt = mysqli_prepare($link, $query)) {
                call_user_func_array(array($stmt, 'bind_param'), $bind_params);
                if (!mysqli_stmt_execute($stmt)) {
                    $update = false;
                }
                mysqli_stmt_close($stmt);
            }
            else {
                $update = false;
            }
            if ($update) {
                // Update isslp majors
                if ($stmt = mysqli_prepare($link, "DELETE FROM isslp_majors WHERE netid=?;")) {
                    mysqli_stmt_bind_param($stmt, "s", $_GET["netid"]);
                    mysqli_stmt_execute($stmt);
                    mysqli_stmt_close($stmt);
                    if (!empty($_GET['isslpmajors'])) { 
                        $majors = $_GET['isslpmajors'];
                        foreach ($majors as $m) {
                            if ((!empty($m)) && ($stmt = mysqli_prepare($link, "INSERT INTO isslp_majors VALUES (?, ?)"))) {
                                mysqli_stmt_bind_param($stmt, "si", $_GET['netid'], $m);
                                mysqli_stmt_execute($stmt);
                                mysqli_stmt_close($stmt);
                            }
                        } 
                    }
                }
                // Update isslp minors
                if ($stmt = mysqli_prepare($link, "DELETE FROM isslp_minors WHERE netid=?;")) {
                    mysqli_stmt_bind_param($stmt, "s", $_GET["netid"]);
                    mysqli_stmt_execute($stmt);
                    mysqli_stmt_close($stmt);
                    if (!empty($_GET['isslpminors'])) { 
                        $minors = $_GET['isslpminors'];
                        foreach ($minors as $m) {
                            if ((!empty($m)) && ($stmt = mysqli_prepare($link, "INSERT INTO isslp_minors VALUES (?, ?)"))) {
                                mysqli_stmt_bind_param($stmt, "si", $_GET['netid'], $m);
                                mysqli_stmt_execute($stmt);
                                mysqli_stmt_close($stmt);
                            }
                        } 
                    }
                }
                // Update grad majors
                if ($stmt = mysqli_prepare($link, "DELETE FROM grad_majors WHERE netid=?;")) {
                    mysqli_stmt_bind_param($stmt, "s", $_GET["netid"]);
                    mysqli_stmt_execute($stmt);
                    mysqli_stmt_close($stmt);
                    if (!empty($_GET['gradmajors'])) { 
                        $majors = $_GET['gradmajors'];
                        foreach ($majors as $m) {
                            if ((!empty($m)) && ($stmt = mysqli_prepare($link, "INSERT INTO grad_majors VALUES (?, ?)"))) {
                                mysqli_stmt_bind_param($stmt, "si", $_GET['netid'], $m);
                                mysqli_stmt_execute($stmt);
                                mysqli_stmt_close($stmt);
                            }
                        } 
                    }
                }
                // Update grad minors
                if ($stmt = mysqli_prepare($link, "DELETE FROM grad_minors WHERE netid=?;")) {
                    mysqli_stmt_bind_param($stmt, "s", $_GET["netid"]);
                    mysqli_stmt_execute($stmt);
                    mysqli_stmt_close($stmt);
                    if (!empty($_GET['gradminors'])) { 
                        $minors = $_GET['gradminors'];
                        foreach ($minors as $m) {
                            if ((!empty($m)) && ($stmt = mysqli_prepare($link, "INSERT INTO grad_minors VALUES (?, ?)"))) {
                                mysqli_stmt_bind_param($stmt, "si", $_GET['netid'], $m);
                                mysqli_stmt_execute($stmt);
                                mysqli_stmt_close($stmt);
                            }
                        } 
                    }
                }
                    // Update phone numbers 
                if ($stmt = mysqli_prepare($link, "DELETE FROM student_phones WHERE netid=?;")) {
                    mysqli_stmt_bind_param($stmt, "s", $_GET["netid"]);
                    mysqli_stmt_execute($stmt);
                    mysqli_stmt_close($stmt);
                    if (!empty($_GET['phones'])) { 
                        $phones = $_GET['phones'];
                        foreach ($phones as $p) {
                            if (!empty($p) && $stmt = mysqli_prepare($link, "INSERT INTO student_phones VALUES (?, ?)")) {
                                mysqli_stmt_bind_param($stmt, "ss", $_GET['netid'], $p);
                                mysqli_stmt_execute($stmt);
                                mysqli_stmt_close($stmt);
                            }
                        } 
                    }
                }
                // Update email addresses 
                if ($stmt = mysqli_prepare($link, "DELETE FROM student_emails WHERE netid=?;")) {
                    mysqli_stmt_bind_param($stmt, "s", $_GET["netid"]);
                    mysqli_stmt_execute($stmt);
                    mysqli_stmt_close($stmt);
                    if (!empty($_GET['emails'])) { 
                        $emails = $_GET['emails'];
                        foreach ($emails as $e) {
                            if (!empty($e) && $stmt = mysqli_prepare($link, "INSERT INTO student_emails VALUES (?, ?)")) {
                                mysqli_stmt_bind_param($stmt, "ss", $_GET['netid'], $e);
                                mysqli_stmt_execute($stmt);
                                mysqli_stmt_close($stmt);
                            }
                        } 
                    }
                }
            }
            if ($update) {
                // If successfully updated, display text
                $msg = "<p>You have successfully updated the record for " . $_GET["firstname"] . " "  . $_GET["lastname"] . " ("  . $_GET["netid"] . ") in the database";
            }
            else {
                $msg = "<p>Unable to update the record for" . $_GET["firstname"] . " "  . $_GET["lastname"] . " ("  . $_GET["netid"] . ") in the database";
            }
        }
        // Add new outcome to database
        if (isset($_GET['action']) && $_GET["action"] == "insert_outcome") {
            // Connect to database
            $link = mysqli_connect('localhost', 'sjohns37', 'databases2018', 'sjohns37');
            if (mysqli_connect_errno()) {
                printf("Connect failed: %s\n", mysqli_connect_error());
                exit();
            }
            // Initialize variables
            $query = "";
            $query_vals = "";
            $types = "";
            $params = array();
            $insert = false;

            // Build query, datatypes, and parameters 
            // Netid 
            if (!empty($_GET['netid'])) {
                $types .= "s";
                $params[] = $_GET['netid']; 
                $query .= "netid";
                $query_vals .= "?";
            }
            // Research name
            if (!empty($_GET['researchname'])) {
                $types .= "s";
                $params[] = $_GET['researchname']; 
                if (!empty($query)) {
                    $query .= ", ";
                    $query_vals .= ", ";
                }
                $query .= "research_name";
                $query_vals .= "?";
            }
            // Research type
            if (!empty($_GET['researchtype'])) {
                $types .= "s";
                $params[] = $_GET['researchtype']; 
                if (!empty($query)) {
                    $query .= ", ";
                    $query_vals .= ", ";
                }
                $query .= "research_type";
                $query_vals .= "?";
            }
            // Research description
            if (!empty($_GET['researchdesc'])) {
                $types .= "s";
                $params[] = $_GET['researchdesc']; 
                if (!empty($query)) {
                    $query .= ", ";
                    $query_vals .= ", ";
                }
                $query .= "research_description";
                $query_vals .= "?";
            }
            // Organization
            if (!empty($_GET['organization'])) {
                $types .= "s";
                $params[] = $_GET['organization']; 
                if (!empty($query)) {
                    $query .= ", ";
                    $query_vals .= ", ";
                }
                $query .= "organization";
                $query_vals .= "?"; } 
            //  City 
            if (!empty($_GET['city'])) { 
                $types .= "s";
                $params[] = $_GET['city'];
                if (!empty($query)) { 
                    $query .= ", ";
                    $query_vals .= ", ";
                }
                $query .= "city";
                $query_vals .= "?";
            }
            // Country
            if (!empty($_GET['country'])) { 
                $types .= "s";
                $params[] = $_GET['country'];
                if (!empty($query)) { 
                    $query .= ", ";
                    $query_vals .= ", ";
                }
                $query .= "country";
                $query_vals .= "?";
            }
            // Year
            if (!empty($_GET['year'])) { 
                $types .= "i";
                $params[] = $_GET['year'];
                if (!empty($query)) { 
                    $query .= ", ";
                    $query_vals .= ", ";
                }
                $query .= "research_date";
                $query_vals .= "?";
            }
            // Presented at 
            if (!empty($_GET['presented'])) { 
                $types .= "s";
                $params[] = $_GET['presented'];
                if (!empty($query)) { 
                    $query .= ", ";
                    $query_vals .= ", ";
                }
                $query .= "presented_at";
                $query_vals .= "?";
            }
            // Faculty member
            if (!empty($_GET['faculty'])) { 
                $types .= "s";
                $params[] = $_GET['faculty'];
                if (!empty($query)) { 
                    $query .= ", ";
                    $query_vals .= ", ";
                }
                $query .= "faculty_member";
                $query_vals .= "?";
            }
            // Notes 
            if (!empty($_GET['notes'])) { 
                $types .= "s";
                $params[] = $_GET['notes'];
                if (!empty($query)) { 
                    $query .= ", ";
                    $query_vals .= ", ";
                }
                $query .= "notes";
                $query_vals .= "?";
            }
            // Build query and store parameters in array (by reference)
            $query = "INSERT INTO research (" . $query  . ") VALUES (" . $query_vals  . ");";
            $bind_params = array();
            $bind_params[] = & $types;
            for ($i = 0; $i < count($params); $i++) {
                $bind_params[] = & $params[$i];
            }
            // Prepare and execute query
            if ($stmt = mysqli_prepare($link, $query)) {
                call_user_func_array(array($stmt, 'bind_param'), $bind_params);
                mysqli_stmt_execute($stmt);
                $insert = true;
                mysqli_stmt_close($stmt);
            }
            // If no students found, display text
            if ($insert) {
                $msg = "<p>You have successfully added a new research project into the database.</p>";
            }
            else {
                $msg = "<p>Error. Unable to insert new research project into the database.</p>";
            }
        }
        // Update outcome in the database
        if (isset($_GET['action']) && $_GET["action"] == "update_outcome") {
            // Connect to database
            $link = mysqli_connect('localhost', 'sjohns37', 'databases2018', 'sjohns37');
            if (mysqli_connect_errno()) {
                printf("Connect failed: %s\n", mysqli_connect_error());
                exit();
            }
            $query = "";
            $types = "";
            $params = array();
            $update = false;

            // Build query, datatypes, and parameters 
            // Research name
            if (!empty($_GET['researchname'])) {
                $types .= "s";
                $params[] = $_GET['researchname']; 
                if (!empty($query)) {
                    $query .= ", ";
                }
                $query .= "research_name=?";
            }
            // Research type
            if (!empty($_GET['researchtype'])) {
                $types .= "s";
                $params[] = $_GET['researchtype']; 
                if (!empty($query)) {
                    $query .= ", ";
                }
                $query .= "research_type=?";
            }
            // Research description
            if (!empty($_GET['researchdesc'])) {
                $types .= "s";
                $params[] = $_GET['researchdesc']; 
                if (!empty($query)) {
                    $query .= ", ";
                }
                $query .= "research_description=?";
            }
            // Organization
            if (!empty($_GET['organization'])) {
                $types .= "s";
                $params[] = $_GET['organization']; 
                if (!empty($query)) {
                    $query .= ", ";
                }
                $query .= "organization=?";
            }
            // City
            if (!empty($_GET['city'])) {
                $types .= "s";
                $params[] = $_GET['city']; 
                if (!empty($query)) {
                    $query .= ", ";
                }
                $query .= "city=?";
            }
            // Country 
            if (!empty($_GET['country'])) {
                $types .= "s";
                $params[] = $_GET['country']; 
                if (!empty($query)) {
                    $query .= ", ";
                }
                $query .= "country=?";
            }
            // Research date 
            if (!empty($_GET['year'])) { 
                $types .= "i";
                $params[] = $_GET['year'];
                if (!empty($query)) { 
                    $query .= ", ";
                }
                $query .= "research_date=?";
            }
            // Presented at 
            if (!empty($_GET['presented'])) { 
                $types .= "s";
                $params[] = $_GET['presented'];
                if (!empty($query)) { 
                    $query .= ", ";
                }
                $query .= "presented_at=?";
            }
            // Faculty member 
            if (!empty($_GET['faculty'])) { 
                $types .= "s";
                $params[] = $_GET['faculty'];
                if (!empty($query)) { 
                    $query .= ", ";
                }
                $query .= "faculty_member=?";
            }
            // Notes 
            if (!empty($_GET['notes'])) { 
                $types .= "s";
                $params[] = $_GET['notes'];
                if (!empty($query)) { 
                    $query .= ", ";
                }
                $query .= "notes=?";
            }
            // Add netid and research id
            if (!empty($_GET['netid'])) {
                $types .= "s";
                $params[] = $_GET['netid']; 
            }
            if (!empty($_GET['researchid'])) {
                $types .= "i";
                $params[] = $_GET['researchid']; 
            }
            // Build query and store parameters in array (by reference)
            $query = "UPDATE research SET " . $query . " WHERE netid=? AND research_id=?;";
            $bind_params = array();
            $bind_params[] = & $types;
            for ($i = 0; $i < count($params); $i++) {
                $bind_params[] = & $params[$i];
            }
            // Prepare and execute query
            if ($stmt = mysqli_prepare($link, $query)) {
                call_user_func_array(array($stmt, 'bind_param'), $bind_params);
                mysqli_stmt_execute($stmt);
                // If successfully updated display text
                $msg = "<p>You have successfully updated the research in the database.</p>";
                mysqli_stmt_close($stmt);
            }
            else {
                $msg = "<p>Update to update the research in the database.</p>";
            }
        }
        // Delete outcome
        if (isset($_GET['action']) && $_GET["action"] == "delete_outcome") {
            $link = mysqli_connect('localhost', 'sjohns37', 'databases2018', 'sjohns37');
            if (mysqli_connect_errno()) {
                printf("Connect failed: %s\n", mysqli_connect_error());
                exit();
            }
            if ($stmt = mysqli_prepare($link, "UPDATE research SET is_deleted=1 WHERE netid=? AND research_id=?")) {
                mysqli_stmt_bind_param($stmt, "si", $_GET['netid'], $_GET['researchid']);
                mysqli_stmt_execute($stmt);
                mysqli_stmt_close($stmt);
            }
        }
        // Connect to database
        $link = mysqli_connect('localhost', 'sjohns37', 'databases2018', 'sjohns37');
        if (mysqli_connect_errno()) {
            printf("Connect failed: %s\n", mysqli_connect_error());
            exit();
        }
        // Get all student information
        $netid = $_GET['netid'];
        if ($stmt = mysqli_prepare($link, "SELECT netid, first_name, last_name, isslp_year, site_name, grad_year, students.notes FROM students, sites WHERE netid=? AND students.site_id=sites.site_id")) {
            mysqli_stmt_bind_param($stmt, "s", $netid);
            mysqli_stmt_execute($stmt);
            mysqli_stmt_bind_result($stmt, $netid, $firstname, $lastname, $isslpyear, $site, $gradyear, $notes);
            mysqli_stmt_fetch($stmt);
            mysqli_stmt_close($stmt);
        }
        printf("<div class='jumbotron'>");
        printf("<div class='glass'><h1>%s %s (%s)</h1></div>", $firstname, $lastname, $netid); // Logan: made one change here to put header in div for glass effect
        printf("</div>");
        echo "<div class='msg'>" . $msg . "</div>";
        ?>
        <div class="container col-md-4 col-md-offset-4">
            <h3>STUDENT INFORMATION</h3>
            <table class="table table-striped table-bordered table-sm">
              <tbody>
                <tr>
                  <th scope="row">ISSLP Year</th>
                  <td><?php echo $isslpyear; ?></td>
                </tr>
                <tr>
                  <th scope="row">Site</th>
                  <td><?php echo $site; ?></td>
                <tr>
                <tr>
                  <th scope="row">Graduation Year</th>
                  <td><?php echo $gradyear; ?></td>
                <tr>
        <?php
        // Phone numbers 
        if ($stmt = mysqli_prepare($link, "SELECT phone_number FROM student_phones WHERE netid=?")) {
            mysqli_stmt_bind_param($stmt, "s", $netid);
            mysqli_stmt_execute($stmt);
            mysqli_stmt_bind_result($stmt, $phone);
            printf("<tr><th scope='row'>Phone Number</th><td>");
            while(mysqli_stmt_fetch($stmt)) {
                printf("%s<br>", $phone);
            }
            printf("</td></tr>");
            mysqli_stmt_close($stmt);
        }
        // Email addresses
        if ($stmt = mysqli_prepare($link, "SELECT email FROM student_emails WHERE netid=?")) {
            mysqli_stmt_bind_param($stmt, "s", $netid);
            mysqli_stmt_execute($stmt);
            mysqli_stmt_bind_result($stmt, $email);
            printf("<tr><th scope='row'>Email</th><td>");
            while(mysqli_stmt_fetch($stmt)) {
                printf("%s<br>", $email);
            }
            printf("</td></tr>");
            mysqli_stmt_close($stmt);
        }
        ?>
                <tr>
                  <th scope="row">Notes</th>
                  <td><?php echo $notes; ?></td>
                <tr>
                </tbody>
            </table>
        </div>
        <div class="container col-md-6 col-md-offset-3">
            <h3>AREAS OF STUDY</h3>
            <table class="table table-sm table-striped table-bordered">
                <thead>
                    <tr>
                      <th scope="col"></th>
                      <th scope="col">Majors</th>
                      <th scope="col">Minors</th>
                    </tr>
                </thead>
                <tbody>
        <?php
        // ISSLP majors
        if ($stmt = mysqli_prepare($link, "SELECT major FROM isslp_majors, majors WHERE isslp_majors.major_id=majors.major_id AND netid=?")) {
            mysqli_stmt_bind_param($stmt, "s", $netid);
            mysqli_stmt_execute($stmt);
            mysqli_stmt_bind_result($stmt, $major);
            printf("<tr><th scope='row'>During ISSLP</th><td>");
            while(mysqli_stmt_fetch($stmt)) {
                printf("%s<br>", $major);
            }
            printf("</td>");
            mysqli_stmt_close($stmt);
        }
        // ISSLP minors 
        if ($stmt = mysqli_prepare($link, "SELECT minor FROM isslp_minors, minors WHERE isslp_minors.minor_id=minors.minor_id AND netid=?")) {
            mysqli_stmt_bind_param($stmt, "s", $netid);
            mysqli_stmt_execute($stmt);
            mysqli_stmt_bind_result($stmt, $minor);
            printf("<td>");
            while(mysqli_stmt_fetch($stmt)) {
                printf("%s<br>", $minor);
            }
            printf("</td></tr>");
            mysqli_stmt_close($stmt);
        }
        // Grad majors
        if ($stmt = mysqli_prepare($link, "SELECT major FROM grad_majors, majors WHERE grad_majors.major_id=majors.major_id AND netid=?")) {
            mysqli_stmt_bind_param($stmt, "s", $netid);
            mysqli_stmt_execute($stmt);
            mysqli_stmt_bind_result($stmt, $major);
            printf("<tr><th scope='row'>Upon Graduation</th><td>");
            while(mysqli_stmt_fetch($stmt)) {
                printf("%s<br>", $major);
            }
            printf("</td>");
            mysqli_stmt_close($stmt);
        }
        // Grad majors
        if ($stmt = mysqli_prepare($link, "SELECT minor FROM grad_minors, minors WHERE grad_minors.minor_id=minors.minor_id AND netid=?")) {
            mysqli_stmt_bind_param($stmt, "s", $netid);
            mysqli_stmt_execute($stmt);
            mysqli_stmt_bind_result($stmt, $minor);
            printf("<td>");
            while(mysqli_stmt_fetch($stmt)) {
                printf("%s<br>", $minor);
            }
            printf("</td></tr>");
            mysqli_stmt_close($stmt);
        }
        ?>
                </tbody>
            </table>
        <div class="row">
            <div class="col-xs-3 col-xs-offset-3">
                <form id="update_student" action="update_student.php">
                    <button class="btn btn-default" type="submit"><span class="glyphicon glyphicon-pencil"></span> Update</button>
                    <input type="hidden" name="netid" value=<?php echo $_GET["netid"];?>>
                </form>
            </div>
            <div class="col-xs-3">
                <form id="delete_student" action="index.php" onclick="return confirm('Are you sure you would like to delete this student?');">
                    <button class="btn btn-default" type="submit"><span class="glyphicon glyphicon-trash"></span> Delete</button>
                    <input type="hidden" name="action" value="delete">
                    <input type="hidden" name="netid" value=<?php echo $_GET["netid"];?>>
                </form>
            </div>
        </div>
        <br>
        </div>
        <?php
        printf("
            <div class=\"col-md-10 col-md-offset-1\">
            <h3>RESEARCH PROJECTS</h3>
            <table class=\"table table-sm table-striped table-bordered table-hover\">
                <thead>
                <tr>
                    <th scope=\"col\">Research</th>
                    <th scope=\"col\">Description</th>
                    <th scope=\"col\">Year</th>
                    <th scope=\"col\">Location</th>
                    <th scope=\"col\">Presented</th>
                    <th scope=\"col\">Faculty</th>
                    <th scope=\"col\">Notes</th>
                    <th scope=\"col\">Update</th>
                    <th scope=\"col\">Delete</th>
                </tr>
                </thead>
                <tbody>
        ");

        // Outcomes
        if ($stmt = mysqli_prepare($link, "SELECT research_id, research_name, research_type, research_description, presented_at, research_date, organization, city, country, faculty_member, notes FROM research WHERE netid=? AND is_deleted!=1")) {
            mysqli_stmt_bind_param($stmt, "s", $netid);
            mysqli_stmt_execute($stmt);
            mysqli_stmt_bind_result($stmt, $researchid, $researchname, $researchtype, $researchdesc, $presented, $year, $organization, $city, $country, $faculty, $notes);
            while (mysqli_stmt_fetch($stmt)) {
                printf("
                    <tr>
                        <td>%s<br>(%s)</td>
                        <td>%s</td>
                        <td>%s</td>
                        <td>%s<br>%s<br>%s</td>
                        <td>%s</td>
                        <td>%s</td>
                        <td>%s</td>
                        <td class='text-center'>
                            <form action=\"update_outcome.php\">
                                <input type='hidden' name='action' value='update'>
                                <input type='hidden' name='netid' value=%s>
                                <input type='hidden' name='researchid' value=%s>
                                <button class=\"btn btn-default\" type='submit'>
                                    <span class=\"glyphicon glyphicon-pencil\">
                                    </span>
                                </button>
                            </form>
                        </td>
                        <td class='text-center'>
                            <form action=\"student_action.php\">
                                <input type='hidden' name='action' value='delete_outcome'>
                                <input type='hidden' name='netid' value=%s>
                                <input type='hidden' name='researchid' value=%s>
                                <button class=\"btn btn-default\" type='submit'>
                                    <span class=\"glyphicon glyphicon-trash\">
                                    </span>
                                </button>
                            </form>
                        </td>
                    </tr>
                ", $researchname, $researchtype, $researchdesc, $year, $organization, $city, $country, $presented, $faculty, $notes, $netid, $researchid, $netid, $researchid);
            }
            mysqli_stmt_close($stmt);
        }
    printf("
            <tr>
                <td class='text-center' colspan='9'>
                    <form action=\"update_outcome.php\">
                        <input type='hidden' name='action' value='add_outcome'>
                        <input type='hidden' name='netid' value=%s>
                        <button class=\"btn btn-default\" type='submit'>
                            <span class=\"glyphicon glyphicon-plus\">
                            </span>
                            Add Research 
                        </button>
                    </form>
                </td>
            </tr>
        </tbody>
    </table>
    ", $_GET['netid']);
    ?>
    <br>
    <br>
    <br>
    </body>
</html>
