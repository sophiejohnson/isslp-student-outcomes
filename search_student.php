<!doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>Past ISSLP Participants</title>
        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <link href="https://fonts.googleapis.com/css?family=Nunito+Sans" rel="stylesheet">
        <link href="styles/styles.css?v=1.3" rel="stylesheet">
        <link href="styles/search_students.css?v=1.4" rel="stylesheet">
        <!-- jQuery library -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <!-- Latest compiled JavaScript -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <!-- Load Navbar -->
        <script>
            $(function(){
                $("#includedContent").load("navbar.html");
            });
        </script>
        <script type="text/javascript" src="scripts/search_validation.js"></script>
    </head>
    <body>
        <!-- Navbar -->
        <div id="includedContent"></div>
        <div class="jumbotron">
        <h2>
        SEARCH STUDENTS
        </h2>
        <form id="student_form" action="#" method="GET" onsubmit="return check_form(this);">
            <div class="container">
                <div class="col-md-2">
                </div>
                <div class="row form-group col-md-8">
                    <div class="form-group col-md-4">
                        <label>netID</label>
                        <input class="form-control" type="text" name="netid">
                    </div>
                    <div class="form-group col-md-4">
                        <label>First Name</label>
                        <input class="form-control" type="text" name="firstname">
                    </div>
                    <div class="form-group col-md-4">
                        <label>Last Name</label>
                        <input class="form-control" type="text" name="lastname">
                    </div>
                </div>
                <div class="col-md-2">
                </div>
            </div> 
            <div class="form-group col-md-8 col-md-offset-2">
                <div class="form-row">
                    <!-- Majors -->
                    <div class="form-group col-md-6">
                        <label>Major</label>
                        <select class="form-control" name="major">
                        <?php
                            printf("<option selected disabled></option>");
                            $link = mysqli_connect('localhost', 'sjohns37', 'databases2018', 'sjohns37');
                            if (mysqli_connect_errno()) {
                                printf("Connect failed: %s\n", mysqli_connect_error());
                                exit();
                            }
                            // Major dropdown
                            if ($stmt = mysqli_prepare($link, "SELECT major_id, major FROM majors")) {
                                mysqli_stmt_execute($stmt);
                                mysqli_stmt_bind_result($stmt, $id, $major);
                                while (mysqli_stmt_fetch($stmt)) {
                                    printf("<option value=%s>%s</option>", $id, $major);
                                }
                            }
                        ?>
                        </select>
                    </div>
                    <!-- Minors -->
                    <div class="form-group col-md-6">
                        <label>Minor</label>
                        <select class="form-control" name="minor">
                        <?php
                            printf("<option selected disabled></option>");
                            $link = mysqli_connect('localhost', 'sjohns37', 'databases2018', 'sjohns37');
                            if (mysqli_connect_errno()) {
                                printf("Connect failed: %s\n", mysqli_connect_error());
                                exit();
                            }
                            // Minor dropdown
                            if ($stmt = mysqli_prepare($link, "SELECT minor_id, minor FROM minors")) {
                                mysqli_stmt_execute($stmt);
                                mysqli_stmt_bind_result($stmt, $id, $minor);
                                while (mysqli_stmt_fetch($stmt)) {
                                    printf("<option value=%s>%s</option>", $id, $minor);
                                }
                            }
                        ?>
                        </select>
                    </div>
                </div>
            </div>
            <div class="container">
                <div class="row form-group col-md-10 col-md-offset-1">
                    <div class="form-group col-md-2">
                        <label>ISSLP Year Start</label>
                        <input class="form-control" type="text" name="isslpstartyear" maxlength="4">
                    </div>
                    <div class="form-group col-md-2">
                        <label>ISSLP Year End</label>
                        <input class="form-control" type="text" name="isslpendyear" maxlength="4">
                    </div>
                    <div class="form-group col-md-4">
                        <label>ISSLP Site</label>
                        <select class="form-control" name="site">
                            <option selected disabled hidden></option>
                            <?php
                                $link = mysqli_connect('localhost', 'sjohns37', 'databases2018', 'sjohns37');
                                if (mysqli_connect_errno()) {
                                    printf("Connect failed: %s\n", mysqli_connect_error());
                                    exit();
                                }
                                if ($stmt = mysqli_prepare($link, "SELECT site_id, site_name FROM sites")) {
                                    mysqli_stmt_execute($stmt);
                                    mysqli_stmt_bind_result($stmt, $siteid, $sitename);
                                    while (mysqli_stmt_fetch($stmt)) {
                                        printf("<option value=%s>%s</option>", $siteid, $sitename);
                                    }
                                }
                            ?>
                        </select>
                    </div>
                    <div class="form-group col-md-2">
                        <label>Grad Year Start</label>
                        <input class="form-control" type="text" name="gradstartyear" maxlength="4">
                    </div>
                    <div class="form-group col-md-2">
                        <label>Grad Year End</label>
                        <input class="form-control" type="text" name="gradendyear" maxlength="4">
                    </div>
                </div>
            </div>
            <button class="btn btn-default" type="submit" name="submitbutton">
                <span class="glyphicon glyphicon-search">
                </span>
                Search 
            </button>
        </form>
        <br>
        <br>
    <?php
        // Initialize variables
        $query_start = "SELECT DISTINCT students_sites.netid as netid, first_name, last_name, isslp_year, site_name FROM ((SELECT netid, first_name, last_name, isslp_year, grad_year, students.site_id as site_id, site_name, is_deleted FROM students, sites WHERE students.site_id=sites.site_id) students_sites LEFT OUTER JOIN (SELECT netid, minor_id, major_id FROM ((SELECT majors.netid as netid, minor_id, major_id FROM ((SELECT netid, major_id FROM isslp_majors) UNION (SELECT netid, major_id FROM grad_majors)) majors LEFT OUTER JOIN ((SELECT netid, minor_id FROM isslp_minors) UNION (SELECT netid, minor_id FROM grad_minors)) minors ON majors.netid=minors.netid) UNION (SELECT majors.netid as netid, minor_id, major_id FROM ((SELECT netid, major_id FROM isslp_majors) UNION (SELECT netid, major_id FROM grad_majors)) majors RIGHT OUTER JOIN ((SELECT netid, minor_id FROM isslp_minors) UNION (SELECT netid, minor_id FROM grad_minors)) minors ON majors.netid=minors.netid)) majors_minors) students_majors_minors ON students_sites.netid=students_majors_minors.netid) WHERE ";
        $query = "";
        $types = "";
        $params = array();
        $found_students = false;

        // Build query, datatypes, and parameters 
        if (isset($_GET['submitbutton'])) {
            // If exact isslp year entered, adjust so start and end year match 
            if (!empty($_GET['isslpstartyear']) && empty($_GET['isslpendyear'])) {
                $_GET['isslpendyear'] = $_GET['isslpstartyear'];
            }
            else if (empty($_GET['isslpstartyear']) && !empty($_GET['isslpendyear'])) {
                $_GET['isslpstartyear'] = $_GET['isslpendyear'];
            }
            // If exact grad year entered, adjust so start and end year match 
            if (!empty($_GET['gradstartyear']) && empty($_GET['gradendyear'])) {
                $_GET['gradendyear'] = $_GET['gradstartyear'];
            }
            else if (empty($_GET['gradstartyear']) && !empty($_GET['gradendyear'])) {
                $_GET['gradstartyear'] = $_GET['gradendyear'];
            }
            // Match to netid exactly
            if (!empty($_GET['netid'])) {
                $types .= "s";
                $params[] = $_GET['netid']; 
                $query .= " students_sites.netid=?";
            }
            // Match to first and last name as substrings
            if (!empty($_GET['firstname'])) {
                $types .= "s";
                $params[] = "%" . $_GET['firstname'] . "%"; 
                if (!empty($query)) $query .= " AND";
                $query .= " first_name LIKE ?";
            }
            if (!empty($_GET['lastname'])) {
                $types .= "s";
                $params[] = "%" . $_GET['lastname'] . "%"; 
                if (!empty($query)) $query .= " AND";
                $query .= " last_name LIKE ?";
            }
            // Major
            if (!empty($_GET['major'])) {
                $types .= "i";
                $params[] = $_GET['major']; 
                if (!empty($query)) $query .= " AND";
                $query .= " major_id=?";
            }
            // Minor
            if (!empty($_GET['minor'])) {
                $types .= "i";
                $params[] = $_GET['minor']; 
                if (!empty($query)) $query .= " AND";
                $query .= " minor_id=?";
            }
            // ISSLP year range
            if (!empty($_GET['isslpstartyear'])) {
                $types .= "i";
                $params[] = $_GET['isslpstartyear']; 
                if (!empty($query)) $query .= " AND";
                $query .= " isslp_year>=?";
            }
            if (!empty($_GET['isslpendyear'])) {
                $types .= "i";
                $params[] = $_GET['isslpendyear']; 
                $query .= " AND isslp_year<=?";
            }
            // ISSLP site (substring)
            if (!empty($_GET['site'])) {
                $types .= "i";
                $params[] = $_GET['site']; 
                if (!empty($query)) $query .= " AND";
                $query .= " students_sites.site_id=?";
            }
            // Grad year range
            if (!empty($_GET['gradstartyear'])) {
                $types .= "i";
                $params[] = $_GET['gradstartyear']; 
                if (!empty($query)) $query .= " AND";
                $query .= " grad_year>=?";
            }
            if (!empty($_GET['gradendyear'])) {
                $types .= "i";
                $params[] = $_GET['gradendyear']; 
                $query .= " AND grad_year<=?";
            }
            // Build query and store parameters in array (by reference)
            $query = $query_start . $query . " AND is_deleted!=1";
            $bind_params = array();
            $bind_params[] = & $types;
            for ($i = 0; $i < count($params); $i++) {
                $bind_params[] = & $params[$i];
            }
            // Connect to database
            $link = mysqli_connect('localhost', 'sjohns37', 'databases2018', 'sjohns37');
            if (mysqli_connect_errno()) {
                printf("Connect failed: %s\n", mysqli_connect_error());
                exit();
            }
            // Check if any outcomes
            if ($stmt = mysqli_prepare($link, $query)) {
                call_user_func_array(array($stmt, 'bind_param'), $bind_params);
                mysqli_stmt_execute($stmt);
                mysqli_stmt_bind_result($stmt, $netid, $firstname, $lastname, $isslpyear, $site);
                // Print results
                if (mysqli_stmt_fetch($stmt)) {
                    $found_students = true;
                }
                mysqli_stmt_close($stmt);
            }
            if (!$found_students) {
                printf("<p>No students match your search. Please try again.</p><br>");
            }
            printf("
                <h3>STUDENT INFORMATION</h3>
                <div class='row'>
                <div class=\"col-md-8 col-md-offset-2\">
                <table class=\"table table-sm table-striped table-bordered table-hover\">
                    <thead>
                        <tr>
                            <th scope=\"col\">netid</th>
                            <th scope=\"col\">Name</th>
                            <th scope=\"col\">Site</th>
                            <th scope=\"col\">ISSLP Year</th>
                            <th scope=\"col\">More Info</th>
                        </tr>
                    </thead>
                    <tbody>
            ");
            // Prepare and execute query
            if ($stmt = mysqli_prepare($link, $query)) {
                call_user_func_array(array($stmt, 'bind_param'), $bind_params);
                mysqli_stmt_execute($stmt);
                mysqli_stmt_bind_result($stmt, $netid, $firstname, $lastname, $isslpyear, $site);
                // Print results
                while (mysqli_stmt_fetch($stmt)) {
                    $found_students = true;
                    printf("
                        <tr>
                            <td>%s</td>
                            <td>%s %s</td>
                            <td>%s</td>
                            <td>%s</td>
                            <td>
                                <form action=\"student_action.php\">
                                    <input type='hidden' name='action' value='view'>
                                    <input type='hidden' name='netid' value=%s>
                                    <button class=\"btn btn-default\" type='submit'>
                                        <span class=\"glyphicon glyphicon-user\">
                                        </span>
                                    </button>
                                </form>
                            </td>
                        </tr>
                    ", $netid, $firstname, $lastname, $site, $isslpyear, $netid);
                }
                mysqli_stmt_close($stmt);
            }
            printf("
                        <tr>
                            <td class='text-center' colspan='5'>
                                <form action=\"update_student.php\">
                                    <button class=\"btn btn-default\" type='submit'>
                                        <span class=\"glyphicon glyphicon-plus\">
                                        </span>
                                    Add New Student 
                                    </button>
                                </form>
                            </td>
                        </tr>
                    </tbody>
                </table>
                </div>
                </div>
            ");
        }
        ?>
        </div>
    </body>
</html>

