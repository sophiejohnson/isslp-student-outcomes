<?php
    // value used for unset variables
    define("NOTSET", "-1");

    $link = mysqli_connect('localhost', 'sjohns37', 'databases2018', 'sjohns37');
    if (mysqli_connect_errno()) {
        printf("Connect failed: %s\n", mysqli_connect_error());
        exit();
    }
    // Query for map values
    if ($stmt = mysqli_prepare($link, "SELECT netid, first_name, last_name, site_id,  FROM countries")) {
        mysqli_stmt_execute($stmt);
        mysqli_stmt_bind_result($stmt, $code, $name, $HDI, $students_abroad, $continent);

        // create countries dictionary
        echo "<script text='text/javascript'>\n";

        $country_dict_str = "";
        $country_dict_str .= sprintf("var countries ={\n");

        while (mysqli_stmt_fetch($stmt)) {
            // HDI not set
            if(!isset($HDI))
            {
                $HDI = NOTSET;
            }
            // students_abroad not set
            if(!isset($students_abroad))
            {
                $students_abroad = NOTSET;
            }

            // escape quotes
            $name = str_replace("'", "\'", $name);

            // add entry
            $country_dict_str .= sprintf("%s: {'name':'%s', 'HDI':%s, 'students_abroad':%s, 'continent':'%s'},\n", 
                                            $code, $name, $HDI, $students_abroad, $continent);
        }

        // cut off ending comma
        $country_dict_str = substr($country_dict_str, 0, -2);
        $country_dict_str .= sprintf("\n}\n");

        // write to DOM
        echo $country_dict_str;
        echo "</script>\n";

        mysqli_stmt_close($stmt);
    }
?>
