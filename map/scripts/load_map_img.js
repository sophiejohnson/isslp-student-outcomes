import calc_colors from "./calculate_colors.js";

// link base for hdi
var LINK_BASE = 'http://hdr.undp.org/en/countries/profiles/'

// elem holding the map
var map_box = document.getElementById("map_box");

// count students and sites for each country
for(var code in countries) {
    countries[code]['isslp_students'] = 0;

    for(var netid in students) {
        if(students[netid] == countries[code]['name'])
        {
            countries[code]['isslp_students']++;
        }
    }

    countries[code]['sites'] = 0;

    for(var site_id in sites) {
        if(sites[site_id] == countries[code]['name'])
        {
            countries[code]['sites']++;
        }
    }
}

// connect info button for map
var info_button = document.getElementById("info_button");
info_button.onclick = function(){
    alert("This graph displays the HDI of countries "
            + "through color and the number of students performing "
            + "service through texture density.\n\n"
            + "Underneath the graph, you will also find info "
            + "pertaining to the number of sites and the number of students who "
            + "have participated in ISSLPs at each country.\n\n"
            + "The purpose of this graphical info is to aid in the "
            + "planning of new sites.");
}

// create map
var create_map = function(){
    // clear old map
    map_box.innerHTML = "";

    // get height of chart from outer container
    var chart_height = parseInt(map_box.style.maxHeight.slice(0, -2));

    // removes extra empty space from bottom of map
    var bottom_trim = chart_height * 0.06; 

    console.log(calc_colors.map_data);

    var map = new Datamap({
        scope: 'world',
        element: document.getElementById('map_box'),
        setProjection: function(element) {
            var projection = d3.geoEckert3()
                                .scale(290)
                                .translate([element.offsetWidth / 2, element.offsetHeight / 2 + bottom_trim]);
            var path = d3.geo.path()
                        .projection(projection);
            return {path: path, projection: projection};
        },
        height: chart_height - bottom_trim,
        fills: {defaultFill: d3.hsl(0, 0, calc_colors.light)},
        data: calc_colors.map_data,
        geographyConfig: {
            popupTemplate: function(geo, data) {
                var country_name = document.getElementById("country_name");

                // skip country if doesn't exist
                if(!country_name || !geo || /[1-9]+/.test(geo.id)) return;

                var HDI_value = document.getElementById("HDI");
                var students_value = document.getElementById("students_abroad");
                var isslp_value = document.getElementById("isslp_students");
                var sites_value = document.getElementById("sites");
                var code = geo.id;

                // make highlight string
                var popup_str = code + ' | ';
                popup_str += countries[code]['name'] + ' | ';
                popup_str += 'HDI: ' + countries[code]['HDI'].toFixed(3) + ' | '
                popup_str += 'SA: ' + countries[code]['students_abroad'] + ' | '
                popup_str += 'IS: ' + countries[code]['isslp_students'] + ' | '
                popup_str += 'sites: ' + countries[code]['sites']

                // set values in html
                country_name.innerHTML = geo.properties.name;
                HDI_value.innerHTML = (countries[code]['HDI'] >= 0) ? countries[code]['HDI'].toFixed(3) : 'No Data';
                students_value.innerHTML = (countries[code]['students_abroad'] >= 0) ? countries[code]['students_abroad'] : 'No Data';
                isslp_value.innerHTML = countries[code]['isslp_students'];
                sites_value.innerHTML = countries[code]['sites'];

                return ['<div class="hoverinfo"><strong>',
                       popup_str,
                       '</strong></div>'].join('');
            }
        },
        done: function(datamap) {
            datamap.svg.selectAll('.datamaps-subunit').on('click', function(geo) {
                // do nothing if country doesn't have hdi or doesn't exist
                if(!country_name || !geo || /[1-9]+/.test(geo.id) || 
                            countries[geo.id]['HDI'] < 0)
                {
                    return;
                }
                window.open(LINK_BASE + geo.id, '_blank');
            })
        }
    })

    // get the svg element that the maps in
    var svg = document.querySelector('.datamap');

    var aspect_ratio = 2.4;
    var map_height = map_box.clientHeight;
    var map_width = map_height * aspect_ratio;
    console.log("map_height: " + map_height);
    console.log("map_width: " + map_width);
    svg.style.overflow = "visible";
    svg.style.position = "relative";
    svg.style.left = (map_width - map_box.clientWidth) + "px";
    svg.style.top = 0;
    svg.style.width = map_width;
    svg.style.height = map_height;

    svg.setAttribute("viewBox", "0 0 " + map_width + " " + map_height);
    svg.setAttribute("height", map_height);
    svg.setAttribute("width", map_width);
    svg.setAttribute("data-width", map_width);
    svg.setAttribute("overflow", "visible");
    svg.setAttribute("preserveAspectRatio", "xMidYMid meet");

    // prevents clipping when svg's aspect ratio doesn't match container's
    //svg.setAttribute("width", svg_width + 1000);

    console.log(map_box);
    console.log(map_box.style.width);
    console.log(svg);
}

create_map();
window.addEventListener('resize', create_map);

/*
var svg = d3.select("#textures2").append("svg");
var t = lines().thicker();
svg.append("circle").style("fill", t.url());
*/
