import textures from "../plugins/textures-js/textures.js";
// lowest and highest hue of color range
var hue = 200;
var min_hue = 0; // 100 best
var max_hue = 260; // 335 best

var hue_offset = 15; // 175 best
var hue_range = max_hue - min_hue;
var hue_max_range = 359;

var hue_min_off = min_hue + hue_offset;
var hue_max_off = max_hue + hue_offset;

var saturation = 1.0;
var min_saturation = 0;
var max_saturation = 1.0;

var light = 0.5;
var min_light = 0;
var max_light = 0.8;

// number of colors
var n_colors = 20;
var step_size = 1/n_colors;

// scale function (input between 0 and 1)
/*
var hsl_scale = d3.scaleSequential(function(t) {
    var curr_hue = (t * (max_hue-min_hue)) + min_hue - hue_offset;
    return d3.hsl(curr_hue, saturation, light) + "";
});
*/
var hsl_scale = function(t)
{
    var range_min = hue_min_off / hue_max_range;
    var range_max = hue_max_off / hue_max_range;
    var rainbow_val = ((t/1) * (range_max - range_min)) + range_min;
    return d3.interpolateRainbow(rainbow_val);
}

// find min hdi
var min_hdi = Infinity;
for(var code in countries)
{
    if(countries[code]['HDI'] >= 0 && countries[code]['HDI'] < min_hdi)
    {
        min_hdi = countries[code]['HDI'];
    }
}
console.log("Min HDI: " + min_hdi);

// shades and fill_values dicts for later use
var shades = {};
for(var i = 1; i <= n_colors; i++)
{
    var max_value = 1.0 * i / n_colors;
    // skip unnecessary shades
    if(max_value < min_hdi) continue;

    // value used for color
    var true_max_value = ((max_value/1) * (1 + min_hdi)) - min_hdi;
    shades[max_value + ""] = hsl_scale(true_max_value) + "";
}
console.log(shades);

// find max students
var max_students = -1;
for(var code in countries)
{
    if(countries[code]['students_abroad'] > max_students) {
        max_students = countries[code]['students_abroad'];
    }
}

// general log func
var logger = function(n){
    var BASE = 1.1
    return Math.log(n)/Math.log(BASE);
}

var min_size = 1;
var max_size = 20;
var size_offset = 1 - min_size;
var coef = (max_size+size_offset)/logger(max_students);
console.log(coef);

// scales size of pattern based on number of students
var scaleSize = function(students_abroad) {
    var return_size = ((max_size + size_offset)-(coef * logger(students_abroad+1))) + min_size;
    return return_size;
}

// creates a map between country and shade
var svg = d3.select("#textures").append("svg").attr("width", 0).attr("height", 0);
var map_colors = {}
var map_data = {};

for(var code in countries)
{
    // set color for country
    if(countries[code]['HDI'] >= 0)
    {
        // loops through map divisions of color and checks if the hdi fits in the category
        for(var i = 1; i <= n_colors; i++)
        {
            var max_value = 1.0 * i / n_colors;

            if(countries[code]['HDI'] <= max_value)
            {
                var true_max_value = ((max_value/1) * (1 + min_hdi)) - min_hdi;
                map_colors[code] = hsl_scale(true_max_value);
                break;
            }
        }
    }
    // set to a default grey value
    else
    {
        map_colors[code] = d3.hsl(0, 0, light);
    }

    // set texture in map_data
    // use texture
    if(countries[code]['students_abroad'] > 0)
    {
        var texture =  textures.paths()
                        .d("hexagons")
                        .size(scaleSize(countries[code]['students_abroad']))
                        .strokeWidth(0.5)
                        .stroke('black')
                        .background(map_colors[code])
                        ;
        svg.call(texture);
        map_data[code] = texture.url();
    }
    // use plain color
    else {
        map_data[code] = map_colors[code];
    }
}

console.log("Max students: " + max_students);

console.log(map_data);
export default {shades, map_data, max_students, min_hdi, light, step_size};
