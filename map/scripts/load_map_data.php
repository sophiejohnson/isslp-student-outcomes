<?php
    // value used for unset variables
    define("NOTSET", "-1");

    $link = mysqli_connect('localhost', 'sjohns37', 'databases2018', 'sjohns37');
    if (mysqli_connect_errno()) {
        printf("Connect failed: %s\n", mysqli_connect_error());
        exit();
    }
    // Query for countries
    if ($stmt = mysqli_prepare($link, "SELECT code, name, HDI, students_abroad, continent FROM countries")) {
        mysqli_stmt_execute($stmt);
        mysqli_stmt_bind_result($stmt, $code, $name, $HDI, $students_abroad, $continent);

        // create countries dictionary
        echo "<script text='text/javascript'>\n";

        $country_dict_str = "";
        $country_dict_str .= sprintf("var countries ={\n");

        while (mysqli_stmt_fetch($stmt)) {
            // HDI not set
            if(!isset($HDI))
            {
                $HDI = NOTSET;
            }
            // students_abroad not set
            if(!isset($students_abroad))
            {
                $students_abroad = NOTSET;
            }

            // escape quotes
            $name = str_replace("'", "\'", $name);

            // add entry
            $country_dict_str .= sprintf("%s: {'name':'%s', 'HDI':%s, 'students_abroad':%s, 'continent':'%s'},\n", 
                                            $code, $name, $HDI, $students_abroad, $continent);
        }

        // cut off ending comma
        $country_dict_str = substr($country_dict_str, 0, -2);
        $country_dict_str .= sprintf("\n};\n");

        // write to DOM
        echo $country_dict_str;
        echo "</script>\n";

        mysqli_stmt_close($stmt);
    }
    // Query for sites
    if ($stmt = mysqli_prepare($link, "SELECT site_id, country FROM sites")) {
        mysqli_stmt_execute($stmt);
        mysqli_stmt_bind_result($stmt, $site_id, $country);

        // create countries dictionary
        echo "<script text='text/javascript'>\n";

        $sites_dict_str = "";
        $sites_dict_str .= sprintf("var sites ={\n");

        while (mysqli_stmt_fetch($stmt)) {
            // add entry
            $sites_dict_str .= sprintf("%s: '%s',\n", 
                                            $site_id, $country);
        }

        // cut off ending comma
        $sites_dict_str = substr($sites_dict_str, 0, -2);
        $sites_dict_str .= sprintf("\n};\n");

        // write to DOM
        echo $sites_dict_str;
        echo "</script>\n";

        mysqli_stmt_close($stmt);
    }
    // Query for students
    if ($stmt = mysqli_prepare($link, "SELECT netid, country FROM students 
                                            INNER JOIN sites on 
                                            students.site_id = sites.site_id")) {
        mysqli_stmt_execute($stmt);
        mysqli_stmt_bind_result($stmt, $netid, $country);

        // create countries dictionary
        echo "<script text='text/javascript'>\n";

        $students_dict_str = "";
        $students_dict_str .= sprintf("var students ={\n");

        while (mysqli_stmt_fetch($stmt)) {
            // add entry
            $students_dict_str .= sprintf("'%s': '%s',\n", 
                                            $netid, $country);
        }

        // cut off ending comma
        $students_dict_str = substr($students_dict_str, 0, -2);
        $students_dict_str .= sprintf("\n};\n");

        // write to DOM
        echo $students_dict_str;
        echo "</script>\n";

        mysqli_stmt_close($stmt);
    }
?>
