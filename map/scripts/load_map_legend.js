import calc_colors from "./calculate_colors.js";
var shades = calc_colors.shades;
var step_size = calc_colors.step_size;
console.log(shades);
console.log(step_size);

// holds the legend
var legend_box = document.getElementById("map_legend");

// get the keys in sorted order for the legend elements
var keys = Object.keys(shades).map(parseFloat);
keys.sort();
console.log(keys);
for(var key_index in keys) {
    // retrieve the hdi_value from the keys dict
    var hdi_value = keys[key_index];

    // holds the text for the legend element
    var elem_desc_box = document.createElement("div");
    elem_desc_box.className = "map_legend_elem";
    elem_desc_box.style.paddingLeft = "10px";
    elem_desc_box.style.paddingRight = "5px";

    // the actual text for the legend key
    var elem_text = document.createElement("h2");
    elem_text.classList.add("map_h2");
    elem_text.classList.add("legend_h2");
    elem_text.innerHTML = ((hdi_value-step_size).toPrecision(2)) + 
                                "-" + hdi_value.toPrecision(2) + ":";

    // holds the color for the element
    var elem_color_box = document.createElement("div");
    elem_color_box.className = "map_legend_elem";
    elem_color_box.style.backgroundColor = shades[hdi_value];

    // add the elements to the document
    elem_desc_box.appendChild(elem_text);
    legend_box.appendChild(elem_desc_box);
    legend_box.appendChild(elem_color_box);
}
console.log(legend_box);
