// Script for form validation
function check_form(form) {
    // Record variables 
    var netid = form.netid.value.trim(); 
    var isslpstartyear = form.isslpstartyear.value.trim(); 
    var isslpendyear = form.isslpendyear.value.trim(); 
    var gradstartyear = form.gradstartyear.value.trim(); 
    var gradendyear = form.gradendyear.value.trim(); 
    // Check validity
    var valid = true;
    var message = "Form Submission Error:\n";
    var re = /^[\w ]+$/;
    // NetID must contain no special characters
    if (netid && !re.test(netid)) {
        form.netid.focus();
        message += "\t- NetID must contain only alphanumeric characters.\n";
        valid = false;
    }
    // ISSLP year is 4 digit integer between 1901 and 2155
    var re = /^\d{4}$/;
    var isslpyear = form.isslpstartyear.value.trim();
    if(isslpyear && (!(re.test(isslpyear)) || isslpyear < 1901 || isslpyear > 2155)) {
        form.isslpstartyear.focus();
        message += "\t- The start of the ISSLP Year range must be a valid year between 1901 and 2155.\n"
        valid = false;
    }
    var isslpyear = form.isslpendyear.value.trim();
    if(isslpyear && (!(re.test(isslpyear)) || isslpyear < 1901 || isslpyear > 2155)) {
        form.isslpendyear.focus();
        message += "\t- The end of the ISSLP Year range must be a valid year between 1901 and 2155.\n"
        valid = false;
    }
    // Range must be valid
    if (isslpstartyear && isslpendyear && (isslpstartyear > isslpendyear)) {
        form.isslpstartyear.focus();
        form.isslpendyear.focus();
        message += "\t- The start of the ISSLP Year range must come before the end.\n";
        valid = false;
    }
    // Graduation year is 4 digit integer between 1901 and 2155
    var gradyear = form.gradstartyear.value.trim();
    if(gradyear && (!(re.test(gradyear)) || gradyear < 1901 || gradyear > 2155)) {
        form.gradstartyear.focus();
        message += "\t- The start of the Graduation Year range must be a valid year between 1901 and 2155.\n"
        valid = false;
    }
    var gradyear = form.gradendyear.value.trim();
    if(gradyear && (!(re.test(gradyear)) || gradyear < 1901 || gradyear > 2155)) {
        form.gradendyear.focus();
        message += "\t- The end of the Graduation Year range must be a valid year between 1901 and 2155.\n"
        valid = false;
    }
    // Range must be valid
    if (gradstartyear && gradendyear && (gradstartyear > gradendyear)) {
        form.gradstartyear.focus();
        form.gradendyear.focus();
        message += "\t- The start of the Graduation Year range must come before the end.\n";
        valid = false;
    }
    // Alert for errors
    if (!valid) {
        alert(message);
    }
    return valid;
}
