// Script for form validation 
function check_form(form) {
    var valid = true;
    var message = "Form Submission Error:\n";
    // NetID is required
    if (form.netid.value.trim() == "") {
        form.netid.focus();
        valid = false;
        message += "\t- NetID is a required field.\n"
    }
    // NetID contains alphanumeric characters
    else {
        var re = /^[\w ]+$/;
        if(!re.test(form.netid.value.trim())) {
            form.netid.focus();
            message += "\t- NetID must contain only alphanumeric characters.\n"
            valid = false;
        }
    }
    // Research name is required
    if (form.researchname.value.trim() == "") {
        form.researchname.focus();
        valid = false;
        message += "\t- Research Name is a required field.\n"
    }
    // Research type is required
    if (form.researchtype.value.trim() == "") {
        form.researchtype.focus();
        valid = false;
        message += "\t- Research Type is a required field.\n"
    }
    // Year is 4 digit integer between 1901 and 2155
    var re = /^\d{4}$/;
    var year = form.year.value.trim();
    if(year && (!(re.test(year)) || year < 1901 || year > 2155)) {
        form.year.focus();
        message += "\t- Year must be a valid year between 1901 and 2155.\n"
        valid = false;
    }
    // Alert for errors
    if (!valid) {
        alert(message);            
    }
    return valid;
}
