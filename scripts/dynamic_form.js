// Add new isslp majors input to form
$(function() {
    $(document).on('click', '.isslp-majors-add', function(e) {
        e.preventDefault();
        // Use existing minor input 
        var controlForm = $('.controls #isslp_major_inputs:first'),
        currentEntry = $(this).parents('.entry:first'),
        newEntry = $(currentEntry.clone()).appendTo(controlForm);
        // Update entry
        newEntry.find('input').val('');
        controlForm.find('.entry:not(:last) .isslp-majors-add')
        .removeClass('isslp-majors-add').addClass('isslp-majors-remove')
        .removeClass('btn-success').addClass('btn-danger')
        .html('<span class="glyphicon glyphicon-minus"></span>');
    }).on('click', '.isslp-majors-remove', function(e)
    {
    $(this).parents('.entry:first').remove();

    e.preventDefault();
    return false;
    });
});
// Add new isslp minors input to form
$(function() {
    $(document).on('click', '.isslp-minors-add', function(e) {
        e.preventDefault();
        // Use existing minor input 
        var controlForm = $('.controls #isslp_minor_inputs:first'),
        currentEntry = $(this).parents('.entry:first'),
        newEntry = $(currentEntry.clone()).appendTo(controlForm);
        // Update entry
        newEntry.find('input').val('');
        controlForm.find('.entry:not(:last) .isslp-minors-add')
        .removeClass('isslp-minors-add').addClass('isslp-minors-remove')
        .removeClass('btn-success').addClass('btn-danger')
        .html('<span class="glyphicon glyphicon-minus"></span>');
    }).on('click', '.isslp-minors-remove', function(e)
    {
    $(this).parents('.entry:first').remove();

    e.preventDefault();
    return false;
    });
});
// Add new grad majors input to form
$(function() {
    $(document).on('click', '.grad-majors-add', function(e) {
        e.preventDefault();
        // Use existing minor input 
        var controlForm = $('.controls #grad_major_inputs:first'),
        currentEntry = $(this).parents('.entry:first'),
        newEntry = $(currentEntry.clone()).appendTo(controlForm);
        // Update entry
        newEntry.find('input').val('');
        controlForm.find('.entry:not(:last) .grad-majors-add')
        .removeClass('grad-majors-add').addClass('grad-majors-remove')
        .removeClass('btn-success').addClass('btn-danger')
        .html('<span class="glyphicon glyphicon-minus"></span>');
    }).on('click', '.grad-majors-remove', function(e)
    {
    $(this).parents('.entry:first').remove();

    e.preventDefault();
    return false;
    });
});
// Add new grad minors input to form
$(function() {
    $(document).on('click', '.grad-minors-add', function(e) {
        e.preventDefault();
        // Use existing minor input 
        var controlForm = $('.controls #grad_minor_inputs:first'),
        currentEntry = $(this).parents('.entry:first'),
        newEntry = $(currentEntry.clone()).appendTo(controlForm);
        // Update entry
        newEntry.find('input').val('');
        controlForm.find('.entry:not(:last) .grad-minors-add')
        .removeClass('grad-minors-add').addClass('grad-minors-remove')
        .removeClass('btn-success').addClass('btn-danger')
        .html('<span class="glyphicon glyphicon-minus"></span>');
    }).on('click', '.grad-minors-remove', function(e)
    {
    $(this).parents('.entry:first').remove();

    e.preventDefault();
    return false;
    });
});
// Add new phone input to form
$(function() {
    $(document).on('click', '.phone-add', function(e) {
        e.preventDefault();
        // Use existing phone input 
        var controlForm = $('.controls #phone_inputs:first'),
        currentEntry = $(this).parents('.entry:first'),
        newEntry = $(currentEntry.clone()).appendTo(controlForm);
        // Update entry
        newEntry.find('input').val('');
        controlForm.find('.entry:not(:last) .phone-add')
        .removeClass('phone-add').addClass('phone-remove')
        .removeClass('btn-success').addClass('btn-danger')
        .html('<span class="glyphicon glyphicon-minus"></span>');
    }).on('click', '.phone-remove', function(e)
    {
    $(this).parents('.entry:first').remove();

    e.preventDefault();
    return false;
    });
});
// Add new email input to form
$(function() {
    $(document).on('click', '.email-add', function(e) {
        e.preventDefault();
        // Duplicate existing email input
        var controlForm = $('.controls #email_inputs:first'),
        currentEntry = $(this).parents('.entry:first'),
        newEntry = $(currentEntry.clone()).appendTo(controlForm);
        // Update new entry
        newEntry.find('input').val('');
        controlForm.find('.entry:not(:last) .email-add')
        .removeClass('email-add').addClass('email-remove')
        .removeClass('btn-success').addClass('btn-danger')
        .html('<span class="glyphicon glyphicon-minus"></span>');
    }).on('click', '.email-remove', function(e)
    {
    $(this).parents('.entry:first').remove();

    e.preventDefault();
    return false;
    });
});
