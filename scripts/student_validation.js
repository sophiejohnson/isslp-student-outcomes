// Script for form validation 
function check_form(form) {
    var valid = true;
    var message = "Form Submission Error:\n";
    // NetID is required
    if (form.netid.value.trim() == "") {
        form.netid.focus();
        valid = false;
        message += "\t- NetID is a required field.\n"
    }
    // NetID contains alphanumeric characters
    else {
        var re = /^[\w ]+$/;
        if(!re.test(form.netid.value.trim())) {
            form.netid.focus();
            message += "\t- NetID must contain only alphanumeric characters.\n"
            valid = false;
        }
    }
    // First name is required
    if (form.firstname.value.trim() == "") {
        form.firstname.focus();
        message += "\t- First Name is a required field.\n"
        valid = false;
    }
    // Last name is required
    if (form.lastname.value.trim() == "") {
        form.lastname.focus();
        message += "\t- Last Name is a required field.\n"
        valid = false;
    }
    // ISSLP Year is required
    if (form.isslpyear.value.trim() == "") {
        form.isslpyear.focus();
        message += "\t- ISSLP Year is a required field.\n"
        valid = false;
    }
    // ISSLP year is 4 digit integer between 1901 and 2155
    else {
        var re = /^\d{4}$/;
        var isslpyear = form.isslpyear.value.trim();
        if(isslpyear && (!(re.test(isslpyear)) || isslpyear < 1901 || isslpyear > 2155)) {
            form.isslpyear.focus();
            message += "\t- ISSLP Year must be a valid year between 1901 and 2155.\n"
            valid = false;
        }
        var re = /^\d{4}$/;
        if(!re.test(form.isslpyear.value.trim())) {
            form.isslpyear.focus();
            message += "\t- ISSLP Year must be a valid year.\n"
            valid = false;
        }
    }
    // Site is required
    if (form.site.value.trim() == "") {
        form.site.focus();
        message += "\t- ISSLP Site is a required field.\n"
        valid = false;
    }
    // Graduation year is 4 digit integer between 1901 and 2155
    var re = /^\d{4}$/;
    var gradyear = form.gradyear.value.trim();
    if(gradyear && (!(re.test(gradyear)) || gradyear < 1901 || gradyear > 2155)) {
        form.gradyear.focus();
        message += "\t- Graduation Year must be a valid year between 1901 and 2155.\n"
        valid = false;
    }
    var gradyear = form.gradyear.value.trim();
    if(gradyear && (!(re.test(gradyear)) || gradyear < 1901 || gradyear > 2155)) {
        form.gradyear.focus();
        message += "\t- Graduation Year must be a valid year between 1901 and 2155.\n"
        valid = false;
    }
    // Phone numbers
    var re = /^\d+$/;
    var phones = document.getElementsByName('phones[]');
    for (var i = 0; i < phones.length; i++) {
        if(phones[i].value.trim() && !(re.test(phones[i].value.trim()))) {
            message += "\t- Phone numbers must contain only numeric characters.\n"
            valid = false;
        }
    }
    // Emails 
    var re = /^[\w ]+@[\w ]+.[\w ]+$/;
    var emails = document.getElementsByName('emails[]');
    for (var i = 0; i < emails.length; i++) {
        if(emails[i].value.trim() && !(re.test(emails[i].value.trim()))) {
            message += "\t- Email addresses must contain all alphanumeric characters with '@' and '.' characters.\n"
            valid = false;
        }
    }
    // Alert for errors
    if (!valid) {
        alert(message);            
    }
    return valid;
}
