<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Past ISSLP Participants</title>

    <!-- Latest compiled and minified CSS -->
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Nunito+Sans" rel="stylesheet">
    <link href="styles/styles.css?v=1.3" rel="stylesheet">

    <!-- Styles for chart colors --> 
    <link href="chart/css/chart.css?v=1.2" rel="stylesheet">

    <!-- jQuery library -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <!-- Latest compiled JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <!-- Link to d3 js library-->
    <script src="https://d3js.org/d3.v5.min.js"></script>

    <!-- Billboard Library -->
    <script src="https://naver.github.io/billboard.js/release/latest/dist/billboard.js" text="text/javascript"></script>
    
    <!-- Load Navbar -->
    <script>
        $(function(){
            $("#includedContent").load("navbar.html");
        });
    </script>

    <!-- makes dict of dict with country as key and to {'name': value, 'ODA': 117 etc --> 
    <?php include './chart/scripts/load_chart_data.php';?>

</head>

<body>
    <!-- Navbar -->
    <div id="includedContent"></div>

    <div id="chart_header" class="text-center">
        <h2>Development Indicators</h2> 

        <div id="dropdown_container" class="dropdown btn-group">
            <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" onchange="">
                Choose Continent
                <span class="caret"></span>  
            </button>
            <ul id="dropdown_list" class="dropdown-menu">
            </ul>
        </div>
        <h2 id="continent_title"><h2>
    </div>


<!--        <h1 id="testHeader"></h2> -->

    <!-- Bounding container for chart -->
    <div id="chart_box" class="background">
    </div>

    <div id="row">
        <h2>Country Compare<h2>
    </div>
    <div id="row">
        <div id="compare_box">
        </div>
    </div>

    <!-- scripts -->
    <script text="text/javascript" src="chart/scripts/load_dropdown.js"></script>
    <script text="text/javascript" src="chart/scripts/calculate_maxes.js"></script>
    <script text="text/javascript" src="chart/scripts/dropdown_expand.js"></script>
    <script text="text/javascript" src="chart/scripts/load_mini_chart.js"></script>
    <script text="text/javascript" src="chart/scripts/do_billboards.js"></script>
    <script text="text/javascript" src="chart/scripts/load_country_compare.js"></script>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
</body>
</html>
