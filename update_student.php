<!doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>Past ISSLP Participants</title>
        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <link href="https://fonts.googleapis.com/css?family=Nunito+Sans" rel="stylesheet">
        <link href="styles/styles.css?v=1.1" rel="stylesheet">
        <link href="styles/index.css?v=1.1" rel="stylesheet">
        <!-- jQuery library -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <!-- Latest compiled JavaScript -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <!-- Load Navbar -->
        <script>
            $(function(){
                $("#includedContent").load("navbar.html");
            });
        </script>
    </head>
    <body>
        <!-- Navbar -->
        <div id="includedContent"></div>
        <?php 
            // If update, autofill form
            if (isset($_GET['netid'])) {
                $update = true;
                // Connect to database
                $link = mysqli_connect('localhost', 'sjohns37', 'databases2018', 'sjohns37');
                if (mysqli_connect_errno()) {
                    printf("Connect failed: %s\n", mysqli_connect_error());
                    exit();
                }
                $netid = $_GET['netid'];
                // Get information for existing student
                if ($stmt = mysqli_prepare($link, "SELECT netid, first_name, last_name, isslp_year, site_id, grad_year, students.notes FROM students WHERE netid=?")) {
                    mysqli_stmt_bind_param($stmt, "s", $netid);
                    mysqli_stmt_execute($stmt);
                    mysqli_stmt_bind_result($stmt, $netid, $firstname, $lastname, $isslpyear, $site, $gradyear, $notes);
                    mysqli_stmt_fetch($stmt);
                }
            }
            // If not, set boolean to false
            else {
                $update = false;
            }
            if ($update) {
                printf("
                    <div class=\"col-md-12 back-button\">
                    <a class=\"btn btn-default\" href=\"student_action.php?action=view&netid=%s\">Back to Student</a>
                    </div>
                ", $_GET['netid']);
            }
        ?>
        <div class="col-lg-3"></div>
        <div class="container center-col-form col-lg-6">
            <h2>
            <?php echo ($update)? "UPDATE" : "ADD";?> STUDENT
            </h2>
            <p>
            Please enter information below to <?php echo ($update)? "update" : "add";?> student record.
            </p>
            <form id="student_form" action="student_action.php" method="GET" onsubmit="return check_form(this);">
            <input type="hidden" name="action" <?php if ($update) {echo "value=update";} else {echo "value=insert";}?>>
                <div class="form-row">
                    <div class="form-group col-md-12">
                        <label>netID</label>
                        <input type="text" class="form-control" name="netid" maxlength="10" <?php if ($update) {echo "value='".$netid."' readonly";} ?>>
                    </div>
                </div>
                <div class="form-group col-md-12">
                    <div class="row">
                        <div class="form-group col-md-6">
                            <label>First Name</label>
                            <input type="text" class="form-control" name="firstname" <?php if ($update) {echo "value='".$firstname."'";} ?>>
                        </div>
                        <div class="form-group col-md-6">
                            <label>Last Name</label>
                            <input type="text" class="form-control" name="lastname" <?php if ($update) {echo "value='".$lastname."'";} ?>>
                        </div>
                    </div>
                </div>
                <div class="form-group col-md-12">
                    <div class="row">
                        <div class="form-group col-md-3">
                            <label>ISSLP Year</label>
                            <input type="text" class="form-control" name="isslpyear" maxlength="4" <?php if ($update) {echo "value='".$isslpyear."'";} ?>>
                        </div>
                        <div class="form-group col-md-6">
                            <label>ISSLP Site</label>
                            <select class="form-control" name="site" <?php if ($update) {echo "value='".$site."'";} ?>>
                                <?php
                                    if (!$update) {
                                        printf("<option selected disabled hidden></option>");
                                    }
                                    $link = mysqli_connect('localhost', 'sjohns37', 'databases2018', 'sjohns37');
                                    if (mysqli_connect_errno()) {
                                        printf("Connect failed: %s\n", mysqli_connect_error());
                                        exit();
                                    }
                                    // Sites dropdown
                                    if ($stmt = mysqli_prepare($link, "SELECT site_id, site_name FROM sites")) {
                                        mysqli_stmt_execute($stmt);
                                        mysqli_stmt_bind_result($stmt, $siteid, $sitename);
                                        while (mysqli_stmt_fetch($stmt)) {
                                            if ($update && $siteid==$site) {
                                                printf("<option selected value=%s>%s</option>", $siteid, $sitename);
                                            }
                                            else {
                                                printf("<option value=%s>%s</option>", $siteid, $sitename);
                                            }
                                        }
                                    }
                                ?>
                            </select>
                        </div>
                        <div class="form-group col-md-3">
                            <label>Grad Year</label>
                            <input type="text" class="form-control" name="gradyear" maxlength="4" <?php if ($update) {echo "value='".$gradyear."'";} ?>>
                        </div>
                    </div>
                </div>
                <div class="form-group col-md-12">
                    <div class="row">
                        <!-- ISSLP majors -->
                        <div class="form-group col-md-6">
                            <div class="control-group">
                                <div class="controls"> 
                                    <div id="isslp_major_inputs">
                                        <label>ISSLP Majors</label>
                                        <?php
                                        // Connect to database
                                        $link = mysqli_connect('localhost', 'sjohns37', 'databases2018', 'sjohns37');
                                        if (mysqli_connect_errno()) {
                                            printf("Connect failed: %s\n", mysqli_connect_error());
                                            exit();
                                        }
                                        // Update with existing majors 
                                        if ($update) {
                                            if ($stmt = mysqli_prepare($link, "SELECT major_id FROM isslp_majors WHERE netid=?")) {
                                                mysqli_stmt_bind_param($stmt, "s", $_GET['netid']);
                                                mysqli_stmt_execute($stmt);
                                                mysqli_stmt_bind_result($stmt, $majorid);
                                                while (mysqli_stmt_fetch($stmt)) {
                                                    printf("
                                                    <div class=\"entry input-group col-md-12\">
                                                        <select class=\"form-control\" name=\"isslpmajors[]\">
                                                    ");
                                                    // Connect to database
                                                    $link1 = mysqli_connect('localhost', 'sjohns37', 'databases2018', 'sjohns37');
                                                    if (mysqli_connect_errno()) {
                                                        printf("Connect failed: %s\n", mysqli_connect_error());
                                                        exit();
                                                    }
                                                    if ($stmt1 = mysqli_prepare($link1, "SELECT major_id, major FROM majors")) {
                                                        mysqli_stmt_execute($stmt1);
                                                        mysqli_stmt_bind_result($stmt1, $id, $major);
                                                        while (mysqli_stmt_fetch($stmt1)) {
                                                            if ($id == $majorid) {
                                                                printf("<option selected value=%s>%s</option>", $id, $major);
                                                            }
                                                            else {
                                                                printf("<option value=%s>%s</option>", $id, $major);
                                                            }
                                                        }
                                                    }
                                                    printf("
                                                        </select>
                                                        <span class=\"input-group-btn\">
                                                            <button class=\"btn btn-danger btn-remove isslp-majors-remove\" type=\"button\">
                                                                <span class=\"glyphicon glyphicon-minus\"></span>
                                                            </button>
                                                        </span>
                                                    </div>
                                                    ");
                                                }
                                            }
                                        }
                                        printf("
                                        <div class=\"entry input-group col-md-12\">
                                            <select class=\"form-control\" name=\"isslpmajors[]\">
                                        ");
                                            // Connect to database
                                            $link = mysqli_connect('localhost', 'sjohns37', 'databases2018', 'sjohns37');
                                            if (mysqli_connect_errno()) {
                                                printf("Connect failed: %s\n", mysqli_connect_error());
                                                exit();
                                            }
                                            printf("<option selected></option>");
                                            // Minor dropdown
                                            if ($stmt = mysqli_prepare($link, "SELECT major_id, major FROM majors")) {
                                                mysqli_stmt_execute($stmt);
                                                mysqli_stmt_bind_result($stmt, $id, $major);
                                                while (mysqli_stmt_fetch($stmt)) {
                                                    printf("<option value=%s>%s</option>", $id, $major);
                                                }
                                            }
                                            printf("
                                            </select>
                                            <span class=\"input-group-btn\">
                                                <button class=\"btn btn-success btn-add isslp-majors-add\" type=\"button\">
                                                    <span class=\"glyphicon glyphicon-plus\"></span>
                                                </button>
                                            </span>
                                        </div>
                                        ");
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- ISSLP minors -->
                        <div class="form-group col-md-6">
                            <div class="control-group">
                                <div class="controls"> 
                                    <div id="isslp_minor_inputs">
                                        <label>ISSLP Minors</label>
                                        <?php
                                        // Connect to database
                                        $link = mysqli_connect('localhost', 'sjohns37', 'databases2018', 'sjohns37');
                                        if (mysqli_connect_errno()) {
                                            printf("Connect failed: %s\n", mysqli_connect_error());
                                            exit();
                                        }
                                        // Update with existing minors
                                        if ($update && ($stmt = mysqli_prepare($link, "SELECT minor_id FROM isslp_minors WHERE netid=?"))) {
                                            mysqli_stmt_bind_param($stmt, "s", $_GET['netid']);
                                            mysqli_stmt_execute($stmt);
                                            mysqli_stmt_bind_result($stmt, $minorid);
                                            while (mysqli_stmt_fetch($stmt)) {
                                                printf("
                                                <div class=\"entry input-group col-md-12\">
                                                    <select class=\"form-control\" name=\"isslpminors[]\">
                                                ");
                                                // Connect to database
                                                $link1 = mysqli_connect('localhost', 'sjohns37', 'databases2018', 'sjohns37');
                                                if (mysqli_connect_errno()) {
                                                    printf("Connect failed: %s\n", mysqli_connect_error());
                                                    exit();
                                                }
                                                if ($stmt1 = mysqli_prepare($link1, "SELECT minor_id, minor FROM minors")) {
                                                    mysqli_stmt_execute($stmt1);
                                                    mysqli_stmt_bind_result($stmt1, $id, $minor);
                                                    while (mysqli_stmt_fetch($stmt1)) {
                                                        if ($id == $minorid) {
                                                            printf("<option selected value=%s>%s</option>", $id, $minor);
                                                        }
                                                        else {
                                                            printf("<option value=%s>%s</option>", $id, $minor);
                                                        }
                                                    }
                                                }
                                                printf("
                                                    </select>
                                                    <span class=\"input-group-btn\">
                                                        <button class=\"btn btn-danger btn-remove isslp-minors-remove\" type=\"button\">
                                                            <span class=\"glyphicon glyphicon-minus\"></span>
                                                        </button>
                                                    </span>
                                                </div>
                                                ");
                                            }
                                        }
                                        ?>
                                        <!-- Add selector -->
                                        <div class="entry input-group col-md-12">
                                            <select class="form-control" name="isslpminors[]">
                                            <?php
                                                // Connect to database
                                                $link = mysqli_connect('localhost', 'sjohns37', 'databases2018', 'sjohns37');
                                                if (mysqli_connect_errno()) {
                                                    printf("Connect failed: %s\n", mysqli_connect_error());
                                                    exit();
                                                }
                                                printf("<option selected></option>");
                                                // Minor dropdown
                                                if ($stmt = mysqli_prepare($link, "SELECT minor_id, minor FROM minors")) {
                                                    mysqli_stmt_execute($stmt);
                                                    mysqli_stmt_bind_result($stmt, $id, $minor);
                                                    while (mysqli_stmt_fetch($stmt)) {
                                                        printf("<option value=%s>%s</option>", $id, $minor);
                                                    }
                                                }
                                            ?>
                                            </select>
                                            <span class="input-group-btn">
                                                <button class="btn btn-success btn-add isslp-minors-add" type="button">
                                                    <span class="glyphicon glyphicon-plus"></span>
                                                </button>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group col-md-12">
                    <div class="row">
                        <!-- Grad majors -->
                        <div class="form-group col-md-6">
                            <div class="control-group">
                                <div class="controls"> 
                                    <div id="grad_major_inputs">
                                        <label>Grad Majors</label>
                                        <?php
                                        // Connect to database
                                        $link = mysqli_connect('localhost', 'sjohns37', 'databases2018', 'sjohns37');
                                        if (mysqli_connect_errno()) {
                                            printf("Connect failed: %s\n", mysqli_connect_error());
                                            exit();
                                        }
                                        // Update with existing majors 
                                        if ($update && ($stmt = mysqli_prepare($link, "SELECT major_id FROM grad_majors WHERE netid=?"))) {
                                            mysqli_stmt_bind_param($stmt, "s", $_GET['netid']);
                                            mysqli_stmt_execute($stmt);
                                            mysqli_stmt_bind_result($stmt, $majorid);
                                            while (mysqli_stmt_fetch($stmt)) {
                                                printf("
                                                <div class=\"entry input-group col-md-12\">
                                                    <select class=\"form-control\" name=\"gradmajors[]\">
                                                ");
                                                // Connect to database
                                                $link1 = mysqli_connect('localhost', 'sjohns37', 'databases2018', 'sjohns37');
                                                if (mysqli_connect_errno()) {
                                                    printf("Connect failed: %s\n", mysqli_connect_error());
                                                    exit();
                                                }
                                                if ($stmt1 = mysqli_prepare($link1, "SELECT major_id, major FROM majors")) {
                                                    mysqli_stmt_execute($stmt1);
                                                    mysqli_stmt_bind_result($stmt1, $id, $major);
                                                    while (mysqli_stmt_fetch($stmt1)) {
                                                        if ($id == $majorid) {
                                                            printf("<option selected value=%s>%s</option>", $id, $major);
                                                        }
                                                        else {
                                                            printf("<option value=%s>%s</option>", $id, $major);
                                                        }
                                                    }
                                                }
                                                printf("
                                                    </select>
                                                    <span class=\"input-group-btn\">
                                                        <button class=\"btn btn-danger btn-remove grad-majors-remove\" type=\"button\">
                                                            <span class=\"glyphicon glyphicon-minus\"></span>
                                                        </button>
                                                    </span>
                                                </div>
                                                ");
                                            }
                                        }
                                        ?>
                                        <!-- Add a new selector -->
                                        <div class="entry input-group col-md-12">
                                            <select class="form-control" name="gradmajors[]">
                                            <?php
                                                // Connect to database
                                                $link = mysqli_connect('localhost', 'sjohns37', 'databases2018', 'sjohns37');
                                                if (mysqli_connect_errno()) {
                                                    printf("Connect failed: %s\n", mysqli_connect_error());
                                                    exit();
                                                }
                                                printf("<option selected></option>");
                                                // Minor dropdown
                                                if ($stmt = mysqli_prepare($link, "SELECT major_id, major FROM majors")) {
                                                    mysqli_stmt_execute($stmt);
                                                    mysqli_stmt_bind_result($stmt, $id, $major);
                                                    while (mysqli_stmt_fetch($stmt)) {
                                                        printf("<option value=%s>%s</option>", $id, $major);
                                                    }
                                                }
                                            ?>
                                            </select>
                                            <span class="input-group-btn">
                                                <button class="btn btn-success btn-add grad-majors-add" type="button">
                                                    <span class="glyphicon glyphicon-plus"></span>
                                                </button>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- Grad minors -->
                        <div class="form-group col-md-6">
                            <div class="control-group">
                                <div class="controls"> 
                                    <div id="grad_minor_inputs">
                                        <label>Grad Minors</label>
                                        <?php
                                        // Connect to database
                                        $link = mysqli_connect('localhost', 'sjohns37', 'databases2018', 'sjohns37');
                                        if (mysqli_connect_errno()) {
                                            printf("Connect failed: %s\n", mysqli_connect_error());
                                            exit();
                                        }
                                        // Update with existing minors
                                        if ($update && ($stmt = mysqli_prepare($link, "SELECT minor_id FROM grad_minors WHERE netid=?"))) {
                                            mysqli_stmt_bind_param($stmt, "s", $_GET['netid']);
                                            mysqli_stmt_execute($stmt);
                                            mysqli_stmt_bind_result($stmt, $minorid);
                                            while (mysqli_stmt_fetch($stmt)) {
                                                printf("
                                                <div class=\"entry input-group col-md-12\">
                                                    <select class=\"form-control\" name=\"gradminors[]\">
                                                ");
                                                // Connect to database
                                                $link1 = mysqli_connect('localhost', 'sjohns37', 'databases2018', 'sjohns37');
                                                if (mysqli_connect_errno()) {
                                                    printf("Connect failed: %s\n", mysqli_connect_error());
                                                    exit();
                                                }
                                                if ($stmt1 = mysqli_prepare($link1, "SELECT minor_id, minor FROM minors")) {
                                                    mysqli_stmt_execute($stmt1);
                                                    mysqli_stmt_bind_result($stmt1, $id, $minor);
                                                    while (mysqli_stmt_fetch($stmt1)) {
                                                        if ($id == $minorid) {
                                                            printf("<option selected value=%s>%s</option>", $id, $minor);
                                                        }
                                                        else {
                                                            printf("<option value=%s>%s</option>", $id, $minor);
                                                        }
                                                    }
                                                }
                                                printf("
                                                    </select>
                                                    <span class=\"input-group-btn\">
                                                        <button class=\"btn btn-danger btn-remove grad-minors-remove\" type=\"button\">
                                                            <span class=\"glyphicon glyphicon-minus\"></span>
                                                        </button>
                                                    </span>
                                                </div>
                                                ");
                                            }
                                        }
                                        ?>
                                        <!-- Add selector -->
                                        <div class="entry input-group col-md-12">
                                            <select class="form-control" name="gradminors[]">
                                            <?php
                                                // Connect to database
                                                $link = mysqli_connect('localhost', 'sjohns37', 'databases2018', 'sjohns37');
                                                if (mysqli_connect_errno()) {
                                                    printf("Connect failed: %s\n", mysqli_connect_error());
                                                    exit();
                                                }
                                                printf("<option selected></option>");
                                                // Minor dropdown
                                                if ($stmt = mysqli_prepare($link, "SELECT minor_id, minor FROM minors")) {
                                                    mysqli_stmt_execute($stmt);
                                                    mysqli_stmt_bind_result($stmt, $id, $minor);
                                                    while (mysqli_stmt_fetch($stmt)) {
                                                        printf("<option value=%s>%s</option>", $id, $minor);
                                                    }
                                                }
                                            ?>
                                            </select>
                                            <span class="input-group-btn">
                                                <button class="btn btn-success btn-add grad-minors-add" type="button">
                                                    <span class="glyphicon glyphicon-plus"></span>
                                                </button>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Phone numbers -->
                <div class="col-md-12 form-group">
                    <div class="control-group">
                        <label class="control-label" for="phone">Phone</label>
                        <div class="controls"> 
                            <div id="phone_inputs">
                                <?php
                                // Connect to database
                                $link = mysqli_connect('localhost', 'sjohns37', 'databases2018', 'sjohns37');
                                if (mysqli_connect_errno()) {
                                    printf("Connect failed: %s\n", mysqli_connect_error());
                                    exit();
                                }
                                // Update with existing phones 
                                if ($update && ($stmt = mysqli_prepare($link, "SELECT phone_number FROM student_phones WHERE netid=?"))) {
                                    mysqli_stmt_bind_param($stmt, "s", $_GET['netid']);
                                    mysqli_stmt_execute($stmt);
                                    mysqli_stmt_bind_result($stmt, $phone);
                                    while (mysqli_stmt_fetch($stmt)) {
                                        printf("
                                        <div class=\"entry input-group col-md-12\">
                                            <input maxlength='10' class=\"form-control\" name=\"phones[]\" type=\"text\" value=%s>
                                            <span class=\"input-group-btn\">
                                                <button class=\"btn btn-danger phone-remove\" type=\"button\">
                                                    <span class=\"glyphicon glyphicon-minus\"></span>
                                                </button>
                                            </span>
                                        </div>
                                        ", $phone);
                                    }
                                }
                                ?>
                                <!-- Add a new input -->
                                <div class="entry input-group col-md-12">
                                    <input class="form-control" maxlength='10' name="phones[]" type="text">
                                    <span class="input-group-btn">
                                        <button class="btn btn-success phone-add" type="button">
                                            <span class="glyphicon glyphicon-plus"></span>
                                        </button>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <br>
                <!-- Emails -->
                <div class="col-md-12 form-group">
                    <div class="control-group">
                        <label class="control-label" for="email">Email</label>
                        <div class="controls"> 
                            <div id="email_inputs">
                                <?php
                                // Connect to database
                                $link = mysqli_connect('localhost', 'sjohns37', 'databases2018', 'sjohns37');
                                if (mysqli_connect_errno()) {
                                    printf("Connect failed: %s\n", mysqli_connect_error());
                                    exit();
                                }
                                // Update with existing emails 
                                if ($update && ($stmt = mysqli_prepare($link, "SELECT email FROM student_emails WHERE netid=?"))) {
                                    mysqli_stmt_bind_param($stmt, "s", $_GET['netid']);
                                    mysqli_stmt_execute($stmt);
                                    mysqli_stmt_bind_result($stmt, $email);
                                    while (mysqli_stmt_fetch($stmt)) {
                                        printf("
                                        <div class=\"entry input-group col-md-12\">
                                            <input class=\"form-control\" name=\"emails[]\" type=\"text\" value=%s>
                                            <span class=\"input-group-btn\">
                                                <button class=\"btn btn-danger email-remove\" type=\"button\">
                                                    <span class=\"glyphicon glyphicon-minus\"></span>
                                                </button>
                                            </span>
                                        </div>
                                        ", $email);
                                    }
                                }
                                ?>
                                <!-- Add a new input -->
                                <div class="entry input-group col-md-12">
                                    <input class="form-control" name="emails[]" type="text">
                                    <span class="input-group-btn">
                                        <button class="btn btn-success email-add" type="button">
                                            <span class="glyphicon glyphicon-plus"></span>
                                        </button>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Notes -->
                <div class="form-row">
                    <div class="form-group col-md-12">
                        <label>Notes</label>
                        <textarea class="form-control" name="notes"><?php if ($update) {echo $notes;} ?></textarea>
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-12">
                        <button class="btn btn-default" type="submit" name="submitbutton">Continue</button>
                    </div>
                </div>
            </form>
        </div>
        <div class="col-lg-3"></div>
        <script type="text/javascript" src="scripts/student_validation.js"></script>
        <script type="text/javascript" src="scripts/dynamic_form.js"></script>
    </body>
</html>

