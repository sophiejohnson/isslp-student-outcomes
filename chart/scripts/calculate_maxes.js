//init
var max_GDP = 0;
var max_ODA = 0;
var max_US_aid = 0; 

for (var country_code in countries) {
  if (countries[country_code]['GDP'] > max_GDP){
    max_GDP = countries[country_code]['GDP'];
  }
    
  if (countries[country_code]['ODA'] > max_ODA){
    max_ODA = countries[country_code]['ODA'];
  }

  if (countries[country_code]['US_aid'] > max_US_aid){
    max_US_aid = countries[country_code]['US_aid'];
  }
}

