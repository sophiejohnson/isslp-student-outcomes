var compare_box = document.getElementById("compare_box");
var n_compares = 2; // number of comparison boxes

var create_compare_div = function(parent_id, id_num){
    // the main container holding all the compare containers
    var parent_div = document.getElementById(parent_id);

    // holds the comparison for one country
    var compare_div = document.createElement("div");
    compare_div.classList.add("col-md-6");
    compare_div.setAttribute("id", "compare_div" + i);
    compare_div.classList.add("compare_container");

    // First row
    var row1_div = document.createElement("div");
    row1_div.classList.add("row");

    // Add select dropdowns for continents 
    var continent_select = document.createElement("select");
    continent_select.classList.add("continent_select");
    continent_select.classList.add("col-md-10");
    continent_select.classList.add("col-md-offset-1");
    continent_select.setAttribute("id", "continent_select" + i);
    continent_select.setAttribute("onchange", "update_country_select(this)");

    // Add default value
    var option = document.createElement("option");
    option.value = 0;
    option.text = "Choose Continent";
    continent_select.add(option);

    // Add continents
    for (var continent in continents) {
        var option = document.createElement("option");
        option.value = continent;
        option.text = continent;
        continent_select.add(option);
    }
    
    // Second row
    var row2_div = document.createElement("div");
    row2_div.classList.add("row");

    // Add empty country dropdown
    var country_select = document.createElement("select");
    country_select.classList.add("country_select");
    country_select.classList.add("col-md-10");
    country_select.classList.add("col-md-offset-1");
    country_select.setAttribute("id", "country_select" + i);
    country_select.setAttribute("style", "visibility:hidden");
    country_select.setAttribute("onchange", "create_mini_chart(this)");
    
    // Third row
    var row3_div = document.createElement("div");
    row3_div.classList.add("row");

    // Add a mini chart area to graph on
    var mini_chart = document.createElement("div");
    mini_chart.classList.add("col-md-10");
    mini_chart.classList.add("col-md-offset-1");
    mini_chart.classList.add("mini_chart");
    mini_chart.setAttribute("id", "mini_chart" + i);
    mini_chart.style.height = "250px";
    
    // Fourth row
    var row4_div = document.createElement("div");
    row4_div.classList.add("row");

    // Add country name
    var country_header = document.createElement("h3");
    country_header.setAttribute("id", "country_header" + i);

    // Append to compare div
    row1_div.appendChild(continent_select);
    compare_div.appendChild(row1_div);
    row2_div.appendChild(country_select)
    compare_div.appendChild(row2_div);
    row4_div.appendChild(country_header);
    compare_div.appendChild(row4_div);
    row3_div.appendChild(mini_chart);
    compare_div.appendChild(row3_div);

    // Append to parent div
    parent_div.appendChild(compare_div);
}

// Update country select when continent selected 
var update_country_select = function(continent_select) {
    
    // Get id number
    var id = continent_select.id;
    var num = id[id.length - 1];

    // Update country select
    var country_select = document.getElementById("country_select" + num);
    country_select.setAttribute("style", "visibility:visible");
    country_select.innerHTML = "";
    
    // Add default value
    var option = document.createElement("option");
    option.value = 0;
    option.text = "Choose Country";
    country_select.add(option);

    // Add options for continent
    cont = continent_select.value;
    for (var index in continents[cont]) {
        var code = continents[cont][index];
        var country = countries[code]['name'];
        var option = document.createElement("option");
        option.value = code;
        option.text = country;
        country_select.add(option);
    }
}

// add divs for country
for(var i = 0; i < n_compares; i++) {
    create_compare_div("compare_box", i);
}
