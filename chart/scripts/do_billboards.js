var i;
var continent_len = continents.length
//first word in these 3 lists is label used by billboard library that makes bar chart
var list_GDP = ['GDP']; 
var list_ODA = ['ODA'];
var list_US_aid = ['Foreign_aid']; //accesses data with key 'US_aid' but technically data is from many foreign countries
var curr_cont_countries = []; //country codes in selected continent
var curr_cont_names = []; //country names in selected continent

var max_cont_GDP = 0;
var max_cont_ODA = 0;
var max_cont_US_aid = 0; 

var cont_choice = url_params.get("continent");
console.log(cont_choice)

console.log(continents)
console.log(countries)


var dropdown_list = document.getElementById("dropdown_list");


for (i = 0; i < continents[cont_choice].length; i++) { 
  var country_code = continents[cont_choice][i];
  curr_cont_countries.push(country_code);
  curr_cont_names.push(countries[country_code]['name']);

  //ternaries return 0 if value is -1 (flag for no data)
  //GDP in now in billions of dollars. Data was millions of dollars.
  list_GDP.push((countries[country_code]['GDP'] >= 0) ? (countries[country_code]['GDP'] / 1000) : 0); 
  list_ODA.push((countries[country_code]['ODA'] >= 0) ? countries[country_code]['ODA'] : 0); 
  list_US_aid.push((countries[country_code]['US_aid'] >= 0) ? (countries[country_code]['US_aid'] / 1000000) : 0); //so compare it in millions of dollars 

  //calculate the max for scaling purposes
  if ((countries[country_code]['GDP'] / 1000) > max_cont_GDP){
    max_cont_GDP = (countries[country_code]['GDP'] / 1000);
  }
    
  if (countries[country_code]['ODA'] > max_cont_ODA){
    max_cont_ODA = countries[country_code]['ODA'];
  }

  if ((countries[country_code]['US_aid'] / 1000000)> max_cont_US_aid){
    max_cont_US_aid = countries[country_code]['US_aid'] / 1000000;
  }
}

console.log(max_cont_GDP)
console.log(list_GDP)

var chart = bb.generate({
    bindto: "#chart_box",
    data: {
        type: "bar",
        columns: [
            list_GDP,
            list_ODA,
            list_US_aid
        ],
        colors: {
            GDP: "rgb(255, 84, 114)",
            ODA: "rgb(27, 191, 202)",
            Foreign_aid: "rgb(108, 246, 92)" //is tied to the first word in the list. The first word in the list - Foreign_aid- should talk about the data accurately because it shows in the pop-up (technically called a tooltip) on the graph
        },
        axes: {
            ODA: "y2"
        }
    },
    axis: {
       x: {
            "tick": {
                "outer": true,
                "format": function(index) {
                    return curr_cont_countries[index];
                }
            }
       },
       y: {
            "min": 0,
            "max": (max_cont_GDP > max_cont_US_aid ? max_cont_GDP : max_cont_US_aid), //US_aid in mils of $, GDP in bils of $ units but use same y axis
            "tick": {
                "outer": true
            },
            padding: 0
        },
        y2: {                      //separate axis for ODA on the right of the graph
            "min": 0,
            "tick": {
                "outer": true
            },
            "show": true,
            "max": max_cont_ODA,
            padding: 0
        },
        labels: {
            "y": "Millions of $ (US_aid) / Billions of $ (GDP)"
        }
    //    labels: {
    //        "x": "COUNTRIES"
    //    }
    },
    tooltip: {
        show: {
            index: [0, curr_cont_countries.length - 1] 
        },
        format: {
            title: function(index) {
                return curr_cont_names[index];
            }
        }
    }
});

