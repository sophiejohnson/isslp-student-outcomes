// creates a chart for a comparison box
var create_mini_chart = function(elem) {
    var country_code = elem.value;
    console.log(country_code);

    var div_id_num = elem.id.substr(-1);
    console.log("Div_id_num " + div_id_num);
    var chart_div = document.getElementById('mini_chart' + div_id_num);
    console.log(chart_div);
    // Set country name in header 
    var country_header = document.getElementById('country_header' + div_id_num);
    country_header.innerHTML = "";
    var t = document.createTextNode(countries[country_code]['name']);     // Create a text node
    country_header.appendChild(t);
    console.log(country_header.text);

    var div_id = chart_div.id;
    console.log("div_id " + div_id);
    chart_div.innerHTML = "";

    var charts = [];

    console.log(max_GDP);
    console.log(max_ODA);
    console.log(max_US_aid);

    var elems = {'GDP': {
                    'value': countries[country_code]['GDP'],
                    'max': max_GDP,
                    'color': 'rgb(255, 84, 114)' 
                },
                'ODA': {
                    'value': countries[country_code]['ODA'],
                    'max': max_ODA,
                    'color': 'rgb(27, 191, 202)' 
                },
                'Aid': {
                    'value': countries[country_code]['US_aid'],
                    'max': max_US_aid,
                    'color': 'rgb(108, 246, 92)' 
                }};

    for(var factor in elems) {
        var single_chart = document.createElement('div');
        chart_div.appendChild(single_chart);

        single_chart.id = div_id + '_' + factor;
        single_chart.classList.add("single_chart");
        single_chart.classList.add("col-md-4");

        var chart = bb.generate({
            bindto: '#' + single_chart.id, // may not need #
            data: {
                type: "bar",
                color: function(color, d) {
                    return elems[factor]['color'];
                },
                columns: [
                    [factor, (elems[factor]['value'] > 0) ? elems[factor]['value'] : 0]
                ],
                // set y-axes
                axes: {
                    x: 'country_code',
                    y: factor
                }
            },
            // set min and max for each data type
            axis: {
                y: {
                    min: 0,
                    max: elems[factor]['max'],
                    show: false,
                    padding: 0
                },
                x: {
                    tick: {
                        format: function(index) { return country_code; },
                        "outer": true
                    }
                }
            },
            // set radius of each bar in graph by ratio
            bar: {
                width: {
                    ratio: 0.1
                }
            },
            resize: {
                auto: true
            },
            size: {
                height: 400,
                width: 100
            }
        });

        console.log("pushing chart");
        console.log(chart.internal.y.domain());
        console.log(elems[factor]['max']);

        charts.push(chart);

    }

    console.log(charts);
}
