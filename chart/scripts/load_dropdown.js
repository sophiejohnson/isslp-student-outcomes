var continents = {};
var cont_names = [];

// go through countries and add them to their respective continents
for(var code in countries)
{
    var continent = countries[code]['continent'];
    if(continent == 'Antarctica') continue;

    // add continent if not yet in dict
    if(!(continent in continents))
    {
        continents[continent] = [];
        cont_names.push(continent);
    }

    // append country to country list for current continent
    continents[continent].push(code);
}
cont_names.sort();

console.log(continents);
console.log(cont_names);


// Add a list elem for each continent with query based on continent
var dropdown_list = document.getElementById("dropdown_list");
for(var key in cont_names)
{
    var list_item = document.createElement("li");
    var cont_link = document.createElement("a");

    // set query of page to the selected continent
    cont_link.href = "?continent=" + cont_names[key];
    cont_link.innerHTML = cont_names[key];

    list_item.append(cont_link);
    dropdown_list.append(list_item);
}

// Add current continent to title of chart
var title = document.getElementById("continent_title");
var chart_box = document.getElementById("chart_box");
const url_params = new URLSearchParams(window.location.search);

var continent = url_params.get("continent");
title.innerHTML = continent; // add title to page if continent is specified

// change background image
if(continent != null)
{
    chart_box.classList.remove("background");
    chart_box.classList.add("table");
}
