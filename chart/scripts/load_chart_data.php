<?php
    // value used for unset variables
    define("NOTSET", "-1");

    $link = mysqli_connect('localhost', 'sjohns37', 'databases2018', 'sjohns37');
    if (mysqli_connect_errno()) {
        printf("Connect failed: %s\n", mysqli_connect_error());
        exit();
    }
    // Query for map values
    if ($stmt = mysqli_prepare($link, "SELECT code, name, ODA, US_aid, GDP, continent FROM countries")) {
        mysqli_stmt_execute($stmt);
        mysqli_stmt_bind_result($stmt, $code, $name, $ODA, $US_aid, $GDP, $continent);

        // create countries dictionary
        echo "<script text='text/javascript'>\n";

        $country_dict_str = "";
        $country_dict_str .= sprintf("var countries ={\n");

        while (mysqli_stmt_fetch($stmt)) {
            // ODA not set
            if(!isset($ODA))
            {
                $ODA = NOTSET;
            }
            // US_aid not set
            if(!isset($US_aid))
            {
                $US_aid = NOTSET;
            }
            if(!isset($GDP))
            {
                $GDP = NOTSET;
            }

            // escape quotes
            $name = str_replace("'", "\'", $name);

            // add entry
            $country_dict_str .= sprintf("%s: {'name':'%s', 'ODA':%s, 'US_aid':%s, 'GDP':%s, 'continent':'%s'},\n",
                                            $code, $name, $ODA, $US_aid, $GDP, $continent);
        }

        // cut off ending comma
        $country_dict_str = substr($country_dict_str, 0, -2);
        $country_dict_str .= sprintf("\n}\n");

        // write to DOM
        echo $country_dict_str;
        echo "</script>\n";

        mysqli_stmt_close($stmt);
    }
?>
