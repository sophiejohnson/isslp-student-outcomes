<!doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>Past ISSLP Participants</title>
        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <link href="https://fonts.googleapis.com/css?family=Nunito+Sans" rel="stylesheet">
        <link href="styles/styles.css?v=1.4" rel="stylesheet">
        <link href="styles/index.css?v=1.3" rel="stylesheet">

        <!-- Frosted Glass Stylesheet -->
        <link href="styles/glass.css" rel="stylesheet">

        <!-- jQuery library -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <!-- Latest compiled JavaScript -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <!-- Load Navbar -->
        <script>
            $(function(){
                $("#includedContent").load("navbar.html");
            });
        </script>
    </head>
    <body>
        <!-- svg for frosted glass -->
        <svg xmlns="http://www.w3.org/2000/svg"  version="1.1">
            <defs>
                <filter id="blur">
                    <feGaussianBlur stdDeviation="5"/>
                </filter>
            </defs>
        </svg>

        <?php
            // If delete student, perform action
            if (isset($_GET['netid']) && isset($_GET['action']) && ($_GET['action'] == 'delete')) {
                // Check connection
                $link = mysqli_connect('localhost', 'sjohns37', 'databases2018', 'sjohns37');
                if (mysqli_connect_errno()) {
                    printf("Connect failed: %s\n", mysqli_connect_error());
                    exit();
                }
                $netid = $_GET['netid'];
                // Get first and last name based on netid to delete
                if ($stmt = mysqli_prepare($link, "SELECT netid, first_name, last_name, is_deleted FROM students WHERE netid=?")) {
                    mysqli_stmt_bind_param($stmt, "s", $netid);
                    mysqli_stmt_execute($stmt);
                    mysqli_stmt_bind_result($stmt, $netid, $firstname, $lastname, $is_deleted);
                    mysqli_stmt_fetch($stmt);
                    mysqli_stmt_close($stmt);
                }
                // Delete record of student
                if (!$is_deleted && ($stmt = mysqli_prepare($link, "UPDATE students SET is_deleted=true WHERE netid=?"))) {
                    mysqli_stmt_bind_param($stmt, "s", $netid);
                    mysqli_stmt_execute($stmt);
                    mysqli_stmt_close($stmt);
                    printf("<script>alert(\"You have successfully deleted the record of %s %s (%s) in the database.\");</script>", $firstname, $lastname, $netid);
                    $_GET['action'] = 'none';
                }
                // Delete majors and minors
                $netid = $_GET["netid"];
                if ($stmt = mysqli_prepare($link, "DELETE FROM isslp_majors WHERE netid=?;")) {
                    mysqli_stmt_bind_param($stmt, "s", $netid);
                    mysqli_execute($stmt);
                    mysqli_stmt_close($stmt);
                }
                if ($stmt = mysqli_prepare($link, "DELETE FROM isslp_minors WHERE netid=?;")) {
                    mysqli_stmt_bind_param($stmt, "s", $netid);
                    mysqli_execute($stmt);
                    mysqli_stmt_close($stmt);
                }
                if ($stmt = mysqli_prepare($link, "DELETE FROM grad_majors WHERE netid=?;")) {
                    mysqli_stmt_bind_param($stmt, "s", $netid);
                    mysqli_execute($stmt);
                    mysqli_stmt_close($stmt);
                }
                if ($stmt = mysqli_prepare($link, "DELETE FROM grad_minors WHERE netid=?;")) {
                    mysqli_stmt_bind_param($stmt, "s", $netid);
                    mysqli_execute($stmt);
                    mysqli_stmt_close($stmt);
                }
                if ($stmt = mysqli_prepare($link, "DELETE FROM student_phones WHERE netid=?;")) {
                    mysqli_stmt_bind_param($stmt, "s", $netid);
                    mysqli_execute($stmt);
                    mysqli_stmt_close($stmt);
                }
                if ($stmt = mysqli_prepare($link, "DELETE FROM student_emails WHERE netid=?;")) {
                    mysqli_stmt_bind_param($stmt, "s", $netid);
                    mysqli_execute($stmt);
                    mysqli_stmt_close($stmt);
                }
            }
        ?>
        <!-- Navbar -->
        <div id="includedContent"></div>
        <div class="background">
            <div class="jumbotron-text text-center">
                <div class="glass">
                    <h1>PAST ISSLP PARTICIPANTS</h1>
                </div>
            </div>
            <div class="jumbotron text-center">
                <div class="row">
                    <div class="col-sm-5 col-sm-offset-1">
                        <div class="text-center">
                            <h2>Add New Student</h2>
                            <p>Another student impacted by the ISSLP? Click here to add them to the database.</p>
                            <form id="add_student" action="update_student.php">
                                <input class="btn btn-default" type="submit" value="Continue">
                            </form>
                            </div>
                        </div>
                        <div class="col-sm-5">
                        <div class="text-center">
                            <h2>Search for Students</h2>
                            <p>Looking for a past ISSLP participant? Click here to learn about student research projects.</p>
                            <form id="search_student" action="search_student.php">
                                <input class="btn btn-default" type="submit" value="Continue">
                            </form>
                        </div>
                    </div>
                </div>
            </div>
<!--
            <div class="jumbotron-text text-center">
                <div class="glass">
                <h1>DATA VISUALIZATION</h1>
                </div>
            </div>
-->
            <div class="jumbotron text-center">
                <div class="row">
                    <div class="col-md-6 col-md-offset-3">
                        <h2>Human Development Indicators</h2>
                        <p>How does international service by students across the nation correspond with the human development index of each country visited? Click here to find out.</p>
                        <form id="go_to_map" action="hdi_map.php">
                            <input class="btn btn-default" type="submit" value="Continue">
                        </form>
                    </div>
                </div>
            </div>
            <div class="jumbotron text-center">
                <div class="row">
                    <div class="col-md-6 col-md-offset-3">
                        <h2>International Aid Analysis</h2>
                        <p>How does the amount of international aid received by a developing country compare to its GDP? Click here to learn more.</p>
                        <form id="go_to_map" action="indicators_chart.php">
                            <input class="btn btn-default" type="submit" value="Continue">
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
