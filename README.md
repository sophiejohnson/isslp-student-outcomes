ISSLP Student Outcomes
======================
- Created web application to record outcomes of students who have previously participated in the International Summer Service Learning Program (ISSLP) at the University of Notre Dame
- Designed tool to allow ISSLP staff to track former students who undertake additional research related to international development
- Implemented two data visualizations which explore the relationship between several development indicators and the prevalence of foreign aid and international service in each country
 
[Demonstration Video] 

[Demonstration Video]: https://www.youtube.com/watch?v=DvVXv4OC_rQ&feature=youtu.be