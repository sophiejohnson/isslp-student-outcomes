<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Past ISSLP Participants</title>
    <link href="https://fonts.googleapis.com/css?family=Nunito+Sans" rel="stylesheet">
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <!-- jQuery library -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <!-- jQuery UI library -->
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <!-- Latest compiled JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <!-- CSS -->
    <link href="styles/styles.css?v=1.2" rel="stylesheet">
    <link href="map/css/map.css?v=1.2" rel="stylesheet">
    <!-- Load Navbar -->
    <script>
        $(function(){
            $("#includedContent").load("navbar.html");
        });
    </script>
    <!-- Link to d3 js library-->
    <script src="https://d3js.org/d3.v3.min.js"></script>
    <script src="https://d3js.org/topojson.v1.min.js"></script>

    <!-- Plugins for datamaps and textures -->
    <script src="map/plugins/datamaps/datamaps.js"></script>
    <script type="module" src="map/plugins/textures-js/textures.js"></script>

    <!-- Projection Libraries -->
    <script src="https://d3js.org/d3-array.v1.min.js"></script>
    <script src="https://d3js.org/d3-geo.v1.min.js"></script>
    <script src="https://d3js.org/d3-geo-projection.v2.min.js"></script>

    <!-- Color Libraries -->
    <script src="https://d3js.org/d3-color.v1.min.js"></script>
    <script src="https://d3js.org/d3-format.v1.min.js"></script>
    <script src="https://d3js.org/d3-interpolate.v1.min.js"></script>
    <script src="https://d3js.org/d3-time.v1.min.js"></script>
    <script src="https://d3js.org/d3-time-format.v2.min.js"></script>
    <script src="https://d3js.org/d3-scale.v2.min.js"></script> 
    <script src="https://d3js.org/d3-scale-chromatic.v1.min.js"></script>

</head>
<body>
    <!-- Navbar -->
    <div id="includedContent"></div>
    
    <!-- Load Data From Database -->
    <?php include './map/scripts/load_map_data.php';?>

    <!-- Header -->
    <h1 class="map_h1">Human Development Index vs. International Service</h1> 

    <!--Info Button-->
    <div id="info_box">
        <button type="button" id="info_button" class="btn btn-default">What's this?</button>
    <div>

    <!-- Map Legend -->
    <div id="map_legend"></div>

    <!-- Container for svg map -->
    <div id="map_box" style="max-height: 580px;"></div>

    <!-- Javascript for Map -->
    <script type="module" text="text/javascript" src="map/scripts/calculate_colors.js"></script>
    <script type="module" text="text/javascript" src="map/scripts/load_map_img.js"></script>
    <script type="module" text="text/javascript" src="map/scripts/load_map_legend.js"></script>

    <!-- Info Containers Below Map -->
    <div class="map_elem_container">
        <div class="map_elem_subcontainer">
            <h2 class="map_h2"><b>HDI</b></h2>
            <div class="map_value">
                <h3 id="HDI"></h3>
            </div>
        </div>
        <div class="map_elem_subcontainer">
            <h2 class="map_h2"><b>Int. Service</b></h2>
            <div class="map_value">
                <h3 id="students_abroad"></h3>
            </div>
        </div>
        <div class="map_elem_subcontainer map_elem_subcontainer_center">
            <h2 class="map_h2"><b>Country</b></h2>
            <div class="map_value">
                <h3 id="country_name"></h3>
            </div>
        </div>
        <div class="map_elem_subcontainer">
            <h2 class="map_h2"><b>No. Students</b></h2>
            <div class="map_value">
                <h3 id="isslp_students"></h3>
            </div>
        </div>
        <div class="map_elem_subcontainer">
            <h2 class="map_h2"><b>No. Sites</b></h2>
            <div class="map_value">
                <h3 id="sites"></h3>
            </div>
        </div>
    </div>
    
    <!-- holds the texture patterns -->
    <div id="textures"></div>
</body>
</html>
