<!doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>Past ISSLP Participants</title>
        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <link href="https://fonts.googleapis.com/css?family=Nunito+Sans" rel="stylesheet">
        <link href="styles/styles.css?v=1.3" rel="stylesheet">
        <link href="styles/index.css?v=1.3" rel="stylesheet">
        <!-- jQuery library -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <!-- Latest compiled JavaScript -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <!-- Load Navbar -->
        <script>
            $(function(){
                $("#includedContent").load("navbar.html");
            });
        </script>
        <!-- Form validation -->
        <script type="text/javascript" src="scripts/outcome_validation.js"></script>
    </head>
    <body>
        <!-- Navbar -->
        <div id="includedContent"></div>
    <?php
        printf("
        <div class=\"col-md-12 back-button\">
        <a class=\"btn btn-default\" href=\"student_action.php?action=view&netid=%s\">Back to Student</a>
        </div>
        ", $_GET['netid']);
    ?>
        <div class="col-lg-2"></div>
        <div class="container center-col-form col-lg-8">
        <h2>
            RESEARCH PROJECT 
        </h2>
    <?php 
    $netid = $_GET['netid'];
    // Get netid to get current research 
    if (isset($_GET['researchid'])) {
        $update = true;
        $researchid = $_GET['researchid'];
        // Connect to database
        $link = mysqli_connect('localhost', 'sjohns37', 'databases2018', 'sjohns37');
        if (mysqli_connect_errno()) {
            printf("Connect failed: %s\n", mysqli_connect_error());
            exit();
        }
        // Get information for existing outcomes 
        if ($stmt = mysqli_prepare($link, "SELECT research_name, research_type, research_description, presented_at, research_date, organization, city, country, faculty_member, notes FROM research WHERE netid=? AND research_id=?")) {
            mysqli_stmt_bind_param($stmt, "si", $netid, $researchid);
            mysqli_stmt_execute($stmt);
            mysqli_stmt_bind_result($stmt, $researchname, $researchtype, $researchdesc, $presented, $year, $organization, $city, $country, $faculty, $notes);
            mysqli_stmt_fetch($stmt);
            mysqli_stmt_close($stmt);
        }
    }
    else {
        $update = false;
    }
    ?>
        <form id="outcome_form" action="student_action.php" method="GET" onsubmit="return check_form(this);">
            <input type="hidden" name="action" <?php if ($update) {echo "value=update_outcome";} else {echo "value=insert_outcome";}?>>
            <input type="hidden" name="netid" value="<?php echo $netid; ?>">
            <input type="hidden" name="researchid" <?php if ($update) {echo "value='".$researchid."'";} ?>>
            <div class="form-group col-md-12">
                <div class="row">
                    <div class="form-group col-md-6">
                        <label>Research Name</label>
                        <input class="form-control" type="text" name="researchname" <?php if ($update) {echo "value='".$researchname."'";} ?>>
                    </div>
                    <div class="form-group col-md-6">
                    <label>Research Type</label>
                    <input class="form-control" type="text" name="researchtype" <?php if ($update) {echo "value='".$researchtype."'";} ?>>
                    </div>
                </div>
            </div>
            <div class="form-group col-md-12">
                <div class="form-row">
                    <label>Research Description</label>
                    <textarea class="form-control" name="researchdesc"><?php if ($update) {echo $researchdesc;} ?></textarea>
                </div>
            </div>
            <div class="form-group col-md-12">
                <div class="row">
                    <div class="form-group col-md-4">
                        <label>Organization</label>
                        <input class="form-control" type="text" name="organization" <?php if ($update) {echo "value='".$organization."'";} ?>>
                    </div>
                    <div class="form-group col-md-4">
                        <label>City</label>
                        <input class="form-control" type="text" name="city" <?php if ($update) {echo "value='".$city."'";} ?>>
                    </div>
                    <div class="form-group col-md-4">
                        <label>Country</label>
                        <input class="form-control" type="text" name="country"  <?php if ($update) {echo "value='".$country."'";} ?>>
                    </div>
                </div>
            </div>
            <div class="form-group col-md-12">
                <div class="row">
                    <div class="form-group col-md-4">
                        <label>Year</label>
                        <input class="form-control" type="text" name="year" maxlength="4" <?php if ($update) {echo "value='".$year."'";} ?>>
                    </div>
                    <div class="form-group col-md-4">
                        <label>Presented At</label>
                        <input class="form-control" type="text" name="presented" <?php if ($update) {echo "value='".$presented."'";} ?>>
                    </div>
                    <div class="form-group col-md-4">
                        <label>Faculty Member</label>
                        <input class="form-control" type="text" name="faculty" <?php if ($update) {echo "value='".$faculty."'";} ?>>
                    </div>
                </div>
            </div>
            <div class="form-group col-md-12">
                <div class="form-row">
                    <label>Notes</label>
                    <textarea class="form-control" name="notes"><?php if ($update) {echo $notes;} ?></textarea>
                </div>
            </div>
                <div class="form-row">
                    <div class="form-group col-md-12">
                        <button class="btn btn-default" type="submit" name="submitbutton">Continue</button>
                    </div>
                </div>
        </form>
        </div>
        <div class="col-lg-2"></div>
    </body>
</html>
